﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Kodo.Graphics.Style
{
    /// <summary> 
    /// Represents the method that will receive the event raised by an unhandled exception. 
    /// </summary>
    public delegate void ExceptionDelegate( Exception exception );

    /// <summary>
    /// A WindowManager.
    /// </summary>
    public class WindowManager : SynchronizationContext, IDisposable
    {
        const uint SynchronizationMessage = Windows.WM_USER + 1;

        uint synchronizationThreadId;
        Queue<InvokeEntry> synchronizationQueue;

        WindowBase mainWindow;
        List<WindowBase> windows;

        Exception unhandledException;
        bool unhandledExceptionOccured;

        /// <summary>
        /// Occurs when the application encounters an unhandled exception.
        /// </summary>
        ExceptionDelegate OnUnhandledException;

        /// <summary>
        /// Create a new instance of <see cref="WindowManager"/>.
        /// </summary>
        public WindowManager( ExceptionDelegate exceptionHandler )
        {
            OnUnhandledException = exceptionHandler;

            try
            {
                GlobalAssets.Create();

                windows = new List<WindowBase>();
                synchronizationQueue = new Queue<InvokeEntry>();

                synchronizationThreadId = Windows.GetCurrentThreadId();
                SetSynchronizationContext( this );
            }
            catch ( Exception e )
            {
                exceptionHandler?.Invoke( e );
            }
        }

        void IDisposable.Dispose()
        {

        }

        internal void AddWindow( WindowBase window )
        {
            try
            {
                windows.Add( window );
            }
            catch ( Exception e )
            {
                OnUnhandledException( e );
            }
        }

        internal void GoingAway( WindowBase window )
        {
            try
            {
                if ( window == mainWindow )
                {
                    Windows.PostQuitMessage( 0 );
                }
                else
                {
                    windows.Remove( window );
                }
            }
            catch ( Exception e )
            {
                OnUnhandledException( e );
            }
        }

        void UnhandledException( Exception exception )
        {
            unhandledException = exception;
            unhandledExceptionOccured = true;

            OnUnhandledException?.Invoke( exception );
        }

        /// <summary>
        /// Run the application.
        /// </summary>
        /// <param name="window">The main <see cref="Window"/>.</param>
        public void Run( WindowBase window )
        {
            try
            {
                mainWindow = window;
                mainWindow.Create();

                Windows.NativeMessage message;

                while ( Windows.GetMessage( out message, IntPtr.Zero, 0, 0 ) )
                {
                    do
                    {
                        if ( message.msg == SynchronizationMessage )
                        {
                            ProcessDispatchQueue();
                        }
                        else if ( message.msg == Windows.WM_ENDSESSION )
                        {
                            return;
                        }
                        else
                        {
                            Windows.TranslateMessage( ref message );
                            Windows.DispatchMessage( ref message );

                            if ( unhandledExceptionOccured || message.msg == Windows.WM_QUIT )
                                return;
                        }

                    } while ( Windows.PeekMessage( out message, IntPtr.Zero, 0, 0, Windows.PeekMessageFlags.PM_REMOVE ) );

                    for ( var i = 0; i < windows.Count; i++ )
                    {
                        windows[ i ].Present();
                    }
                }
            }
            catch ( Exception e )
            {
                UnhandledException( e );
            }
        }

        public override SynchronizationContext CreateCopy()
        {
            return this;
        }

        public override void OperationStarted()
        {
        }

        public override void OperationCompleted()
        {
        }
        //public override int Wait( IntPtr[] waitHandles, bool waitAll, int millisecondsTimeout ) { return 0; }
        public override void Post( SendOrPostCallback d, object state )
        {
            InvokeAsync( d, state );
        }

        public override void Send( SendOrPostCallback d, object state )
        {
            Invoke( d, state );
        }

        public bool GetIsInvokeRequired()
        {
            return synchronizationThreadId != Windows.GetCurrentThreadId();
        }

        public object Invoke( Delegate method )
        {
            return Invoke( method, null );
        }

        public object Invoke( Delegate method, params object[] args )
        {
            var entry = new InvokeEntry( method, args, true );

            lock ( synchronizationQueue )
            {
                // Add the new entry to the queue.
                synchronizationQueue.Enqueue( entry );
            }

            if ( !GetIsInvokeRequired() )
            {
                ProcessDispatchQueue();
            }
            else
            {
                // Post the synchronization message to the main thread.
                Windows.PostThreadMessage( synchronizationThreadId, SynchronizationMessage, IntPtr.Zero, IntPtr.Zero );
            }

            if ( !entry.IsCompleted )
            {
                // Wait indefinitely.
                entry.AsyncWaitHandle.WaitOne( -1, false );
            }

            if ( entry.Exception != null )
            {
                // If there was an exception, throw it here.
                throw entry.Exception;
            }

            return entry.ReturnValue;
        }

        public object InvokeAsync( Delegate method )
        {
            return InvokeAsync( method, null );
        }

        public object InvokeAsync( Delegate method, params object[] args )
        {
            var entry = new InvokeEntry( method, args, true );

            lock ( synchronizationQueue )
            {
                // Add the new entry to the queue.
                synchronizationQueue.Enqueue( entry );
            }

            if ( !GetIsInvokeRequired() )
            {
                ProcessDispatchQueue();
            }
            else
            {
                // Post the synchronization message to the main thread.
                Windows.PostThreadMessage( synchronizationThreadId, SynchronizationMessage, IntPtr.Zero, IntPtr.Zero );
            }

            return entry;
        }

        void ProcessDispatchQueue()
        {
            InvokeEntry dispatchEntry = null;

            lock ( synchronizationQueue )
            {
                // Retrieve the first item from the queue, if any.
                if ( synchronizationQueue.Count > 0 )
                {
                    dispatchEntry = synchronizationQueue.Dequeue();
                }
            }

            // Process the whole queue.
            while ( dispatchEntry != null )
            {
                if ( dispatchEntry.Method != null )
                {
                    try
                    {
                        // Execute the delegate.
                        dispatchEntry.ReturnValue = dispatchEntry.Method.DynamicInvoke( dispatchEntry.Arguments );
                    }
                    catch ( Exception e )
                    {
                        // Catch and store any exception.
                        dispatchEntry.Exception = e.GetBaseException();
                    }
                    finally
                    {
                        dispatchEntry.Complete();
                    }
                }

                lock ( synchronizationQueue )
                {
                    // Retrive the next item from the queue, if any.
                    if ( synchronizationQueue.Count > 0 )
                    {
                        dispatchEntry = synchronizationQueue.Dequeue();
                    }
                    else
                    {
                        dispatchEntry = null;
                    }
                }
            }
        }

        internal class InvokeEntry : IAsyncResult, IDisposable
        {
            public bool Synchronous { get; }
            public object ReturnValue { get; set; }
            public object AsyncState { get { return null; } }
            public object[] Arguments { get; }
            public Delegate Method { get; }
            public Exception Exception { get; set; }

            bool isCompleted;
            ManualResetEvent resetEvent;
            object invokeSyncObject = new object();

            public WaitHandle AsyncWaitHandle
            {
                get
                {
                    if ( resetEvent == null )
                    {
                        lock ( invokeSyncObject )
                        {
                            if ( resetEvent == null )
                            {
                                resetEvent = new ManualResetEvent( false );

                                if ( isCompleted )
                                    resetEvent.Set();
                            }
                        }
                    }

                    return resetEvent;
                }
            }

            public bool CompletedSynchronously
            {
                get { return isCompleted && Synchronous; }
            }

            public bool IsCompleted
            {
                get { return isCompleted; }
            }

            public void Complete()
            {
                lock ( invokeSyncObject )
                {
                    isCompleted = true;

                    if ( resetEvent != null )
                    {
                        resetEvent.Set();
                    }
                }
            }

            public InvokeEntry( Delegate method, object[] args, bool synchronous )
            {
                Method = method;
                Arguments = args;
                Synchronous = synchronous;
            }

            void IDisposable.Dispose()
            {
                resetEvent?.Close();
            }
        }
    }
}
