﻿using System;

namespace Kodo.Graphics.Style
{
    public struct TextStyleAlignment
    {
        public static readonly TextStyleAlignment LeftTop = new TextStyleAlignment( TextAlignment.Leading, TextAlignment.Leading );
        public static readonly TextStyleAlignment LeftCenter = new TextStyleAlignment( TextAlignment.Center, TextAlignment.Leading );
        public static readonly TextStyleAlignment LeftBottom = new TextStyleAlignment( TextAlignment.Trailing, TextAlignment.Leading );

        public static readonly TextStyleAlignment CenterTop = new TextStyleAlignment( TextAlignment.Leading, TextAlignment.Center );
        public static readonly TextStyleAlignment CenterCenter = new TextStyleAlignment( TextAlignment.Center, TextAlignment.Center );
        public static readonly TextStyleAlignment CenterBottom = new TextStyleAlignment( TextAlignment.Trailing, TextAlignment.Center );

        public static readonly TextStyleAlignment RightTop = new TextStyleAlignment( TextAlignment.Leading, TextAlignment.Trailing );
        public static readonly TextStyleAlignment RightCenter = new TextStyleAlignment( TextAlignment.Center, TextAlignment.Trailing );
        public static readonly TextStyleAlignment RightBottom = new TextStyleAlignment( TextAlignment.Trailing, TextAlignment.Trailing );

        public TextAlignment Vertical { get; }
        public TextAlignment Horizontal { get; }

        public TextStyleAlignment( TextAlignment vertical, TextAlignment horizontal )
        {
            Vertical = vertical;
            Horizontal = horizontal;
        }
    }

    /// <summary>
    /// Extension methods for <see cref="string"/>.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Find the <see cref="TextRange"/> of the specified text within the string.
        /// </summary>
        /// <param name="self">The string to look in.</param>
        /// <param name="text">The string to find.</param>
        /// <param name="start">The start index.</param>
        /// <param name="comparison">The comparison type to use.</param>
        /// <returns>The <see cref="TextRange"/>.</returns>
        public static TextRange RangeOf( this string self, string text, int start = 0, StringComparison comparison = StringComparison.Ordinal )
        {
            return new TextRange( self.IndexOf( text, start, comparison ), text.Length );
        }
    }

    /// <summary>
    /// Text format used to render text.
    /// </summary>
    public class TextStyle : IDisposable
    {
        /// <summary>
        /// The default style.
        /// </summary>
        public static readonly TextStyle Default = new TextStyle( "Arial", 11 );

        InlineObject ellipsis;
        TextFormat format;

        TextStyleAlignment alignment = TextStyleAlignment.CenterCenter;

        /// <summary>
        /// Get the <see cref="TextFormat"/> text the style.
        /// </summary>
        public TextFormat Format
        {
            get { return format; }
        }

        /// <summary>
        /// Get or set the vertical alignment text the style.
        /// </summary>
        public TextStyleAlignment Alignment
        {
            get { return alignment; }
            set
            {
                alignment = value;
                format.ParagraphAlignment = (ParagraphAlignment)alignment.Vertical;
                format.TextAlignment = alignment.Horizontal;
            }
        }

        void IDisposable.Dispose()
        {
            format.Dispose();
            ellipsis.Dispose();
        }

        /// <summary>
        /// Create a <see cref="TextStyle"/>.
        /// </summary>
        /// <param name="name">Name text the font.</param>
        /// <param name="size">Size text the font.</param>
        /// <param name="weight">Weight text the font.</param>
        public TextStyle( string name, float size, FontWeight weight = FontWeight.Normal )
        {
            format = new TextFormat( name, weight, FontStyle.Normal, FontStretch.Normal, size, string.Empty );

            var trimming = new Trimming();
            trimming.Delimiter = 0;
            trimming.DelimiterCount = 0;
            trimming.Granularity = TrimmingGranularity.Character;

            ellipsis = format.EllipsisTrimSign();

            format.SetTrimming( trimming, ellipsis );
            format.WordWrapping = WordWrapping.NoWrap;
            format.TextAlignment = alignment.Horizontal;
            format.ParagraphAlignment = (ParagraphAlignment)alignment.Vertical;
        }

        /// <summary>
        /// Analyze a piece text text using the style.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="maxWidth">Maximum width allowed.</param>
        /// <param name="maxHeight">Maximum height allowed.</param>
        public TextMetrics GetTextMetrics( string text, float maxWidth = float.MaxValue, float maxHeight = float.MaxValue )
        {
            // Believe it or not, this try...catch is 100% preventing an AccessViolationException.
            try
            {
                using ( var layout = new TextLayout( text, format, maxWidth, maxHeight ) )
                {
                    return layout.Metrics;
                }
            }
            catch ( Exception ) { throw; }
        }

        /// <summary>
        /// Implicitly convert the style to <see cref="TextFormat"/>.
        /// </summary>
        /// <param name="t">The style.</param>
        public static implicit operator TextFormat( TextStyle t )
        {
            return t.format;
        }
    }
}
