﻿using System;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// A textbox.
    /// </summary>
    public class Textbox : Control
    {
        Timer tickTimer;

        string text = "";

        int textCursor;
        float textCursorPosition;
        bool cursorVisible;

        Rectangle clip;
        Rectangle textClip;

        TimeSpan cursorMoved;

        /// <summary>
        /// Get or set the whether or not the textbox is 'subtle'.
        /// </summary>
        public bool Subtle
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set the text of the textbox.
        /// </summary>
        public string Text
        {
            get { return text; }
            set
            {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;

                if ( text != value )
                {
                    text = value;
                    textCursor = text.Length;
                    Update();
                }
            }
        }

        /// <summary>
        /// Create a new <see cref="Textbox"/>.
        /// </summary>
        /// <param name="window">Parent <see cref="Window"/>.</param>
        public Textbox( Window window )
            : base( window )
        {
            tickTimer = new Timer( OnTick );
            tickTimer.Interval = 200;
        }

        /// <summary>
        /// 
        /// </summary>
        protected internal override void OnGotKeyboard()
        {
            base.OnGotKeyboard();

            tickTimer.Start();
            cursorVisible = true;
            Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        protected internal override void OnLostKeyboard()
        {
            base.OnLostKeyboard();

            tickTimer.Stop();
            cursorVisible = false;
            Refresh();
        }

        void OnTick()
        {
            var now = DateTime.Now.TimeOfDay;

            if ( now - cursorMoved > TimeSpan.FromMilliseconds( 500 ) )
            {
                cursorMoved = now;
                cursorVisible = !cursorVisible;
                Refresh();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mouse"></param>
        protected internal override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            CaptureKeyboard();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        protected internal override void OnCharacter( char c )
        {
            base.OnCharacter( c );

            if ( (char.IsLetterOrDigit( c ) || char.IsPunctuation( c ) || char.IsWhiteSpace( c )) && !char.IsControl( c ) )
            {
                text = text.Insert( textCursor, c.ToString() );

                textCursor++;

                if ( textCursor < 0 )
                    textCursor = 0;
                if ( textCursor > text.Length )
                    textCursor = text.Length;

                cursorMoved = DateTime.Now.TimeOfDay;
                cursorVisible = true;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        protected internal override void OnKeyDown( Key key )
        {
            base.OnKeyDown( key );

            switch ( key )
            {
                case Key.Home:
                    textCursor = 0;
                    cursorMoved = DateTime.Now.TimeOfDay;
                    cursorVisible = true;
                    Update();
                    break;

                case Key.End:
                    textCursor = text.Length;
                    cursorMoved = DateTime.Now.TimeOfDay;
                    cursorVisible = true;
                    Update();
                    break;
                case Key.Back:
                case Key.Delete:
                    if ( text.Length > 0 )
                    {
                        int toRemove = textCursor - 1;

                        if ( key == Key.Delete )
                            toRemove++;

                        if ( toRemove < 0 )
                            break;
                        if ( toRemove >= text.Length )
                            toRemove = text.Length - 1;

                        text = text.Remove( toRemove, 1 );

                        if ( key == Key.Back )
                            textCursor--;

                        if ( textCursor < 0 )
                            textCursor = 0;
                        if ( textCursor > text.Length )
                            textCursor = text.Length;

                        cursorMoved = DateTime.Now.TimeOfDay;
                        cursorVisible = true;
                        Update();
                    }
                    break;
                case Key.ArrowLeft:
                    textCursor--;

                    if ( textCursor < 0 )
                        textCursor = 0;
                    if ( textCursor > text.Length )
                        textCursor = text.Length;

                    cursorMoved = DateTime.Now.TimeOfDay;
                    cursorVisible = true;
                    Update();
                    break;
                case Key.ArrowRight:
                    textCursor++;

                    if ( textCursor < 0 )
                        textCursor = 0;
                    if ( textCursor > text.Length )
                        textCursor = text.Length;

                    cursorMoved = DateTime.Now.TimeOfDay;
                    cursorVisible = true;
                    Update();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected internal override void OnUpdate( Context context )
        {
            clip = new Rectangle( Area.Dimensions );
            textClip = clip.Adjust( -5, -5 ).CenterTo( clip );

            TextMetrics textMetrics;

            if ( textCursor >= text.Length )
            {
                textMetrics = TextStyle.GetTextMetrics( text, textClip.Width, textClip.Height );
            }
            else
            {
                textMetrics = TextStyle.GetTextMetrics( text.Substring( 0, textCursor ), textClip.Width, textClip.Height );
            }

            textCursorPosition = textMetrics.WidthIncludingTrailingWhitespace + 3f;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected internal override void OnDraw( Context context )
        {
            context.AntialiasMode = AntialiasMode.Aliased;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            if ( !Subtle )
            {
                context.FillRectangle( clip, SharedStyle.Foreground );

                SharedBrush.Color = Color.Black;
                context.DrawRectangle( clip, SharedBrush, 2 );
            }

            //SharedBrush.Color = Color.FromAColor( 0.05f, Color.GhostWhite );
            //context.FillRectangle( textClip, SharedBrush );

            if ( !string.IsNullOrEmpty( text ) )
            {
                SharedBrush.Color = Color.GhostWhite;
                TextStyle.Alignment = TextStyleAlignment.LeftCenter;
                context.DrawText( text, TextStyle, textClip, SharedBrush );
            }

            if ( cursorVisible )
            {
                SharedBrush.Color = AccentColor;
                context.DrawLine( new Point( textCursorPosition, textClip.Top + 2 ), new Point( textCursorPosition, textClip.Bottom - 2 ), SharedBrush, 1 );
            }
        }
    }
}
