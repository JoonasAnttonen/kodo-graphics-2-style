﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Kodo.Graphics.Style
{
    /// <summary> 
    /// Represents the state of a <see cref="WindowBase"/>. 
    /// </summary>
    public enum WindowState : uint
    {
        /// <summary> 
        /// Window is in its normal state. 
        /// </summary>
        Normal = Windows.SW_SHOWNORMAL,
        /// <summary> 
        /// Window is minimized. 
        /// </summary>
        Minimized = Windows.SW_SHOWMINIMIZED,
        /// <summary> 
        /// Window is maximized. 
        /// </summary>
        Maximized = Windows.SW_SHOWMAXIMIZED
    }

    /// <summary>
    /// Represents the margings of a <see cref="WindowBase"/>.
    /// </summary>
    public struct WindowMargings
    {
        /// <summary>
        /// Left marging.
        /// </summary>
        public readonly float Left;
        /// <summary>
        /// Top marging.
        /// </summary>
        public readonly float Top;
        /// <summary>
        /// Right marging.
        /// </summary>
        public readonly float Right;
        /// <summary>
        /// Bottom marging.
        /// </summary>
        public readonly float Bottom;

        /// <summary>
        /// Create a new symmetrical instance of <see cref="WindowMargings"/>.
        /// </summary>
        /// <param name="margings">Amount of margin on all sides.</param>
        public WindowMargings( float margings ) : this( margings, margings, margings, margings ) { }
        /// <summary>
        /// Create a new instance of <see cref="WindowMargings"/>.
        /// </summary>
        /// <param name="left">Marging on the left.</param>
        /// <param name="top">Marging on the top.</param>
        /// <param name="right">Marging on the right.</param>
        /// <param name="bottom">Marging on the bottom.</param>
        public WindowMargings( float left, float top, float right, float bottom )
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }

    /// <summary>
    /// Ultimate base class for all windows.
    /// </summary>
    public abstract class WindowBase : IDisposable
    {
        static int WindowCount;
        string className;

        enum FrameState
        {
            Skip, Part, Whole
        }

        enum SuspendState
        {
            None,
            Load,
            LoadControl,
            Update,
            UpdateControl
        }

        struct MonitorInfo
        {
            public readonly Rectangle MonitorArea;
            public readonly Rectangle WorkArea;

            public MonitorInfo( IntPtr hWnd )
            {
                Windows.MONITORINFO info = Windows.MONITORINFO.Make();
                Windows.GetMonitorInfo( Windows.MonitorFromWindow( hWnd, Windows.MONITOR_DEFAULTTONEAREST ), ref info );

                MonitorArea = new Rectangle( info.rcMonitor.left, info.rcMonitor.top, info.rcMonitor.right, info.rcMonitor.bottom );
                WorkArea = new Rectangle( info.rcWork.left, info.rcWork.top, info.rcWork.right, info.rcWork.bottom );
            }
        }

        HandleRef wndProcRef;
        WndProcDelegate wndProcDelegate;

        int windowClass;
        IntPtr windowHandle;

        SuspendState suspendedState;
        FrameState frameState;
        DirtyRect[] frameRects;

        Context renderingContext;

        WindowManager manager;
        WindowSettings settings;
        WindowState windowState;

        List<ControlBase> controls = new List<ControlBase>();

        ControlBase controlThatHasMouse;
        ControlBase controlThatHasKeyboard;

        bool locked;
        bool visible = true;
        bool mouseTracking;
        bool mouseIsDown;

        internal IntPtr Handle => windowHandle;

        /// <summary>
        /// Get the <see cref="WindowSettings"/> used to create this <see cref="WindowBase"/>.
        /// </summary>
        protected WindowSettings Settings
        {
            get { return settings; }
        }

        /// <summary>
        /// Get the <see cref="ControlBase"/>s associated with this <see cref="WindowBase"/>s.
        /// </summary>
        public List<ControlBase> Controls
        {
            get { return controls; }
        }

        /// <summary>
        /// Indicates whether or not the <see cref="WindowBase"/> has been created.
        /// </summary>
        protected bool IsCreated
        {
            get { return windowHandle != IntPtr.Zero; }
        }

        /// <summary> 
        /// Get or set whether or not the window is locked. 
        /// </summary>
        public bool Locked
        {
            get { return locked; }
            set
            {
                if ( locked != value )
                {
                    locked = value;

                    if ( windowHandle != IntPtr.Zero )
                    {
                        if ( !locked && mouseTracking )
                        {
                            mouseTracking = false;
                        }

                        Draw();
                    }
                }
            }
        }

        /// <summary> 
        /// Get or set the window visibility. 
        /// </summary>
        public bool Visible
        {
            get { return visible; }
            set
            {
                if ( visible != value )
                {
                    visible = value;

                    if ( windowHandle != IntPtr.Zero )
                    {
                        Windows.ShowWindow( windowHandle, value ? Windows.SW_SHOW : Windows.SW_HIDE );
                    }
                }
            }
        }

        /// <summary> 
        /// Get or set the window title. 
        /// </summary>
        public string Title
        {
            get { return settings.Name; }
            set
            {
                if ( settings.Name != value )
                {
                    if ( windowHandle != IntPtr.Zero )
                    {
                        settings.Name = value;

                        Windows.SetWindowText( windowHandle, settings.Name );
                    }
                }
            }
        }

        /// <summary> 
        /// Get or set the window area. 
        /// </summary>
        public Rectangle Area
        {
            get { return settings.Area; }
            set
            {
                if ( settings.Area != value )
                {
                    var newDimensions = value.Dimensions;

                    if ( settings.Area.Dimensions != value.Dimensions )
                    {
                        if ( newDimensions.Width < settings.MinimumSize.Width ||
                             newDimensions.Height < settings.MinimumSize.Height )
                        {
                            settings.Area = new Rectangle( value.Location, settings.MinimumSize );
                        }
                        else
                        {
                            settings.Area = value;
                        }

                        if ( windowHandle != IntPtr.Zero && windowState != WindowState.Minimized )
                        {
                            renderingContext.Resize( value.Dimensions );
                            Update();
                            Present();
                        }
                    }
                    else
                    {
                        settings.Area = value;
                    }
                }
            }
        }

        /// <summary> 
        /// The <see cref="WindowMargings"/> of the window.
        /// </summary>
        public WindowMargings Margins
        {
            get { return settings.Margings; }
        }

        /// <summary> 
        /// The <see cref="WindowManager"/> of the window.
        /// </summary>
        public WindowManager Manager
        {
            get { return manager; }
        }

        /// <summary>
        /// The <see cref="WindowState"/> of the window.
        /// </summary>
        public WindowState State
        {
            get { return windowState; }
        }

        /// <summary> 
        /// Indicates whether or not an invoke call is required. 
        /// </summary>
        protected bool InvokeRequired
        {
            get { return manager.GetIsInvokeRequired(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected object Invoke( Delegate method )
        {
            return manager.Invoke( method );
        }
        protected object Invoke( Delegate method, params object[] args )
        {
            return manager.Invoke( method, args );
        }
        protected object InvokeAsync( Delegate method )
        {
            return manager.InvokeAsync( method );
        }
        protected object InvokeAsync( Delegate method, params object[] args )
        {
            return manager.InvokeAsync( method, args );
        }

        /// <summary> 
        /// Create a new <see cref="WindowBase"/> with the default settings. 
        /// </summary>
        /// <param name="manager">The WindowManager that this Window belongs to.</param>
        public WindowBase( WindowManager manager )
            : this( manager, new WindowSettings() )
        {
        }

        /// <summary> 
        /// Create a new <see cref="WindowBase"/> with the specified settings. 
        /// </summary>
        /// <param name="manager">The <see cref="WindowManager"/> that this Window belongs to. </param>
        /// <param name="settings">The <see cref="WindowSettings"/> for this Window. </param>
        public WindowBase( WindowManager manager, WindowSettings settings )
        {
            this.manager = manager;
            this.settings = settings;

            // Do this so that the area calculations will actually take place in the Area property.
            var area = settings.Area;
            this.settings.Area = new Rectangle();
            Area = area;
        }

        void IDisposable.Dispose()
        {
            Release();
        }

        /// <summary> 
        /// Create the window. 
        /// </summary>
        public void Create()
        {
            if ( !IsCreated )
            {
                manager.AddWindow( this );

                CreateWindowClass();
                CreateWindowHandle();
                CreateWindowContext();

                Load();

                OnCreated();
            }
        }

        void Release()
        {
            if ( IsCreated )
            {
                ReleaseWindowContext();
                ReleaseWindowHandle();
                ReleaseWindowClass();
            }
        }

        /// <summary> 
        /// Minimize the window. 
        /// </summary>
        public void Minimize()
        {
            Windows.CloseWindow( windowHandle );
        }

        /// <summary> 
        /// Close the window. 
        /// </summary>
        public void Close()
        {
            Windows.DestroyWindow( windowHandle );
        }

        internal void FocusMouse( ControlBase control )
        {
            if ( control == null )
                throw new ArgumentNullException( "control" );

            if ( !IsCreated )
                return;

            if ( controlThatHasMouse != null )
            {
                controlThatHasMouse.OnMouseLeave( new Mouse() );
                controlThatHasMouse.HasMouse = false;
            }

            controlThatHasMouse = control;
            controlThatHasMouse.HasMouse = true;
        }

        internal void Defocus( ControlBase control )
        {
            if ( control == null )
                throw new ArgumentNullException( "control" );

            if ( !IsCreated )
                return;

            if ( controlThatHasMouse == control )
            {
                controlThatHasMouse.OnMouseLeave( new Mouse() );
                controlThatHasMouse.HasMouse = false;
                controlThatHasMouse = null;
            }
        }

        internal void FocusKeyboard( ControlBase control )
        {
            if ( control == null )
                throw new ArgumentNullException( "control" );

            if ( !IsCreated )
                return;

            // Make sure that the window has keyboard focus.
            Windows.SetFocus( windowHandle );

            // Clear the old focus.
            if ( controlThatHasKeyboard != null )
                controlThatHasKeyboard.HasKeyboard = false;

            // Set the new focus.
            controlThatHasKeyboard = control;
            controlThatHasKeyboard.HasKeyboard = true;
        }

        internal void ResetFocus()
        {
            controlThatHasKeyboard = null;

            if ( controlThatHasMouse != null )
            {
                controlThatHasMouse.HasMouse = false;
                controlThatHasMouse.OnMouseLeave( new Mouse() );
                controlThatHasMouse = null;

                /*var mouse = new Mouse();
                mouse.Position = FromScreenToWindow( Mouse.GetPosition() );

                // Find the control that the mouse is on.
                for ( var i = controls.Count - 1; i >= 0; i-- )
                {
                    var control = controls[ i ];

                    if ( control.Visible == true &&
                         control.Locked == false &&
                         control.Area.Contains( mouse.Position ) )
                    {
                        controlThatHasMouse = control;
                        controlThatHasMouse.OnMouseEnter( mouse );
                    }
                }*/
            }
        }

        /// <summary> Map screen coordinates to window coordinates. </summary>
        /// <param name="point"> The Point to convert. </param>
        protected internal Point FromScreenToWindow( Point point )
        {
            var p = new Windows.POINT( (int)point.X, (int)point.Y );
            Windows.MapWindowPoints( IntPtr.Zero, windowHandle, ref p, 1 );
            return new Point( p.x, p.y );
        }
        /// <summary> Map window coordinates to screen coordinates. </summary>
        /// <param name="point"> The Point to convert. </param>
        protected internal Point FromWindowToScreen( Point point )
        {
            var p = new Windows.POINT( (int)point.X, (int)point.Y );
            Windows.MapWindowPoints( windowHandle, IntPtr.Zero, ref p, 1 );
            return new Point( p.x, p.y );
        }

        protected virtual void OnCreated() { }
        protected virtual bool OnClosing() { return true; }

        protected abstract void OnLoad( Context context );
        protected abstract void OnUpdate( Context context );
        protected abstract void OnDraw( Context context );
        protected virtual void OnPostDraw( Context context ) { }
        protected virtual void OnDrawLocked( Context context ) { }

        protected virtual void OnDragDropped( List<string> dropped ) { }
        protected virtual bool OnMouseEnter( Mouse mouse ) { return false; }
        protected virtual bool OnMouseHover( Mouse mouse ) { return false; }
        protected virtual bool OnMouseMove( Mouse mouse ) { return false; }
        protected virtual bool OnMouseLeave( Mouse mouse ) { return false; }
        protected virtual bool OnMouseUp( Mouse mouse ) { return false; }
        protected virtual bool OnMouseDown( Mouse mouse ) { return false; }
        protected virtual bool OnMouseWheel( Mouse mouse ) { return false; }
        protected virtual bool OnMouseDoubleClick( Mouse mouse ) { return false; }

        protected virtual bool OnKeyUp( Key key ) { return false; }
        protected virtual bool OnKeyDown( Key key ) { return false; }

        protected virtual bool OnCharacter( char c ) { return false; }
        protected virtual bool OnHitTest( Point point ) { return false; }

        /// <summary> Set the icon that appears in the taskbar. Can only be set during runtime when the window has been created. </summary>
        /// <param name="icon">The icon.</param>
        protected void SetIcon( Icon icon )
        {
            if ( IsCreated )
            {
                Windows.SendMessage( windowHandle, Windows.WM_SETICON, new IntPtr( Windows.ICON_SMALL ), icon?.Handle ?? IntPtr.Zero );
                Windows.SendMessage( windowHandle, Windows.WM_SETICON, new IntPtr( Windows.ICON_BIG ), icon?.Handle ?? IntPtr.Zero );
            }
        }

        /// <summary> Add a control to this window. Can be added during runtime. Shame on the one who adds a control when it is already added. Don't do it. Seriously. </summary>
        /// <param name="control">The control.</param>
        protected internal void AddControl( ControlBase control )
        {
            controls.Add( control );

            if ( IsCreated )
            {
                Load( control );
            }
        }

        void Reorder()
        {
            var controlStack = new Stack<ControlBase>( controls.Count );
            var controlList = new List<ControlBase>( controls.Count );

            foreach ( var ctrl in controls )
            {
                if ( ctrl.AlwaysOnTop )
                {
                    controlStack.Push( ctrl );
                }
                else
                {
                    controlList.Add( ctrl );
                }
            }

            while ( controlStack.Count > 0 )
                controlList.Add( controlStack.Pop() );

            controls = controlList;
        }

        /// <summary>
        /// Complete reload of the window.
        /// </summary>
        protected internal void Load()
        {
            if ( Suspend( SuspendState.Load ) )
            {
                frameRects = new DirtyRect[ controls.Count ];

                Reorder();

                OnLoad( renderingContext );

                for ( var i = 0; i < controls.Count; i++ )
                {
                    controls[ i ].OnLoad( renderingContext );
                }

                Resume();
            }
        }

        /// <summary>
        /// Reload only the specified control.
        /// </summary>
        /// <param name="control">Control to load.</param>
        public void Load( ControlBase control )
        {
            if ( Suspend( SuspendState.LoadControl ) )
            {
                control.OnLoad( renderingContext );
                Resume();
                Update( control );
            }
        }

        /// <summary> 
        /// Logical update (update and redraw) of the window. 
        /// </summary>
        public void Update()
        {
            if ( Suspend( SuspendState.Update ) )
            {
                OnUpdate( renderingContext );

                for ( var i = 0; i < controls.Count; i++ )
                {
                    controls[ i ].OnUpdate( renderingContext );
                }

                Resume();
            }
        }

        /// <summary>
        /// Logically update only the specified control.
        /// </summary>
        /// <param name="control">Control to update.</param>
        public void Update( ControlBase control )
        {
            if ( Suspend( SuspendState.UpdateControl ) )
            {
                control.OnUpdate( renderingContext );
                Resume();
                Draw( control );
            }
        }

        protected void Draw()
        {
            frameState = FrameState.Whole;
        }

        public void Draw( ControlBase control )
        {
            if ( control.Visible && frameState != FrameState.Whole )
            {
                frameState = FrameState.Part;
                control.RenderThisFrame = true;
            }
        }

        internal void Present()
        {
            if ( visible == false || windowState == WindowState.Minimized )
                return;

            if ( locked )
                frameState = FrameState.Whole;

            if ( frameState == FrameState.Whole )
            {
                renderingContext.BeginDraw();
                renderingContext.Clear( Color.White );
                renderingContext.SetTransform( Matrix3x2.Identity );

                OnDraw( renderingContext );

                for ( var i = 0; i < controls.Count; i++ )
                {
                    var control = controls[ i ];

                    if ( control.Visible )
                    {
                        renderingContext.SetTransform( control.Transform );
                        control.OnDraw( renderingContext );
                        control.RenderThisFrame = false;
                    }
                }

                if ( locked )
                {
                    renderingContext.SetTransform( Matrix3x2.Identity );
                    OnDrawLocked( renderingContext );
                }

                renderingContext.SetTransform( Matrix3x2.Identity );
                OnPostDraw( renderingContext );

                renderingContext.EndDraw();
            }
            else if ( frameState == FrameState.Part )
            {
                renderingContext.BeginDraw();

                var dirties = 0;

                for ( var i = 0; i < controls.Count; i++ )
                {
                    var control = controls[ i ];

                    if ( control.Visible && (control.RenderThisFrame || control.AlwaysOnTop) )
                    {
                        frameRects[ dirties++ ] = new DirtyRect(
                            (int)control.Area.Left,
                            (int)control.Area.Top,
                            (int)control.Area.Right,
                            (int)control.Area.Bottom );

                        renderingContext.SetTransform( control.Transform );
                        control.OnDraw( renderingContext );

                        control.RenderThisFrame = false;
                    }
                }

                if ( dirties > 0 )
                    renderingContext.EndDraw( frameRects, dirties );
                else
                    renderingContext.EndDraw();
            }

            frameState = FrameState.Skip;
        }

        void CreateWindowClass()
        {
            className = string.Format( "kodo-graphics-2-window-{0}", Interlocked.Increment( ref WindowCount ) );

            wndProcDelegate = WndProc;
            wndProcRef = new HandleRef( wndProcDelegate, Marshal.GetFunctionPointerForDelegate( wndProcDelegate ) );

            Windows.WNDCLASSEX classOptions = Windows.WNDCLASSEX.Make();
            classOptions.style = Windows.CS_HREDRAW | Windows.CS_VREDRAW | Windows.CS_DBLCLKS | Windows.CS_OWNDC;
            classOptions.lpfnWndProc = wndProcRef.Handle;
            classOptions.hInstance = Windows.GetModuleHandle( null );
            classOptions.hCursor = Windows.LoadCursor( IntPtr.Zero, 32512 );
            classOptions.lpszClassName = className;

            windowClass = Windows.RegisterClassEx( ref classOptions );
            Utility.ThrowIfZero( windowClass );
        }

        void ReleaseWindowClass()
        {
            if ( windowClass != 0 )
            {
                Windows.UnregisterClass( className, Windows.GetModuleHandle( null ) );
            }
        }

        void CreateWindowHandle()
        {
            var location = settings.Area.Location;
            var dimensions = settings.Area.Dimensions;

            var style = (Windows.WindowStyles.WS_OVERLAPPEDWINDOW | Windows.WindowStyles.WS_CLIPCHILDREN | Windows.WindowStyles.WS_VISIBLE) & ~Windows.WindowStyles.WS_SYSMENU;
            var exStyle = Windows.WindowStylesEx.WS_EX_APPWINDOW;

            if ( settings.ToolWindow )
                exStyle = Windows.WindowStylesEx.WS_EX_TOOLWINDOW | Windows.WindowStylesEx.WS_EX_TOPMOST;

            if ( settings.AcceptFiles )
                exStyle |= Windows.WindowStylesEx.WS_EX_ACCEPTFILES;

            if ( !visible )
                style = style & ~Windows.WindowStyles.WS_VISIBLE;

            if ( string.IsNullOrEmpty( settings.Name ) )
                settings.Name = "kodo-graphics-2-style";

            windowHandle = Windows.CreateWindowEx(
                exStyle,
                className,
                settings.Name,
                style,
                (int)location.X, (int)location.Y,
                (int)dimensions.Width, (int)dimensions.Height,
                IntPtr.Zero,
                IntPtr.Zero,
                Windows.GetModuleHandle( null ),
                IntPtr.Zero );

            Utility.ThrowIfNull( windowHandle );
        }

        void ReleaseWindowHandle()
        {
            windowHandle = IntPtr.Zero;
        }

        void CreateWindowContext()
        {
            renderingContext = new Context( windowHandle );
        }

        void ReleaseWindowContext()
        {
        }

        bool Suspend( SuspendState state )
        {
            if ( windowHandle == IntPtr.Zero )
                return false;

            if ( suspendedState != SuspendState.None )
            {
                return false;
            }

            suspendedState = state;
            return true;
        }

        void Resume()
        {
            if ( suspendedState == SuspendState.Load )
            {
                suspendedState = SuspendState.None;
                Update();
            }
            else if ( suspendedState == SuspendState.Update )
            {
                suspendedState = SuspendState.None;
                Draw();
            }

            suspendedState = SuspendState.None;
        }

        // OnWMDROPFILES
        //______________________________________________________________________________________

        void OnWMDROPFILES( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            // Get the list length.
            var dropCount = Windows.DragQueryFile( wParam, 0xFFFFFFFF, null, 0 );
            var dropFilename = new StringBuilder( 256 );
            var dropFilenameLength = 0u;
            var dropList = new List<string>();

            for ( var dropIndex = 0u; dropIndex < dropCount; dropIndex++ )
            {
                // Get the filename length.
                dropFilenameLength = Windows.DragQueryFile( wParam, dropIndex, null, 0 );
                dropFilenameLength++; // For some reason the returned length is 1 too short.

                // Get the actual filename.
                Windows.DragQueryFile( wParam, dropIndex, dropFilename, dropFilenameLength );
                dropList.Add( dropFilename.ToString() );
            }

            // Finish the drop.
            Windows.DragFinish( wParam );
            OnDragDropped( dropList );
        }

        // OnWMMOUSEUP
        //______________________________________________________________________________________

        void OnWMMOUSEUP( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );
            mouseIsDown = false;
            Windows.ReleaseCapture();

            if ( locked || OnMouseUp( mouse ) )
                return;

            if ( controlThatHasMouse != null )
            {
                mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                controlThatHasMouse.OnMouseUp( mouse );
            }
        }

        // OnWMMOUSEDOWN
        //______________________________________________________________________________________

        void OnWMMOUSEDOWN( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );
            mouseIsDown = true;
            Windows.SetCapture( hWnd );

            if ( locked || OnMouseDown( mouse ) )
                return;

            if ( controlThatHasMouse != null )
            {
                mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                controlThatHasMouse.OnMouseDown( mouse );
            }
        }

        void OnWMMOUSEHOVER( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );

            if ( locked || OnMouseHover( mouse ) )
                return;

            if ( controlThatHasMouse != null )
            {
                mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                controlThatHasMouse.OnMouseHover( mouse );
            }

            // Do this to restore the mouse tracking when the mouse moves again.
            mouseTracking = false;
        }

        // OnWMMOUSEMOVE
        //______________________________________________________________________________________

        void OnWMMOUSEMOVE( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            if ( locked )
                return;

            var mouse = new Mouse( msg, wParam, lParam );

            if ( !mouseTracking )
            {
                // Start tracking the mouse so we can receive the mouse leave event.
                Windows.TRACKMOUSEEVENT trackEvent = new Windows.TRACKMOUSEEVENT( Windows.TMEFlags.TME_LEAVE | Windows.TMEFlags.TME_HOVER, hWnd, 0 );
                Windows.TrackMouseEvent( ref trackEvent );
                mouseTracking = true;

                if ( OnMouseEnter( mouse ) )
                    return;
            }
            else if ( OnMouseMove( mouse ) )
            {
                return;
            }

            if ( controlThatHasMouse != null )
            {
                if ( controlThatHasMouse.HasMouse )
                {
                    var foundBetterFit = false;

                    if ( !controlThatHasMouse.Area.Contains( mouse.Position ) )
                    {
                        // Find the control that the mouse is on.
                        for ( var i = controls.Count - 1; i >= 0; i-- )
                        {
                            var control = controls[ i ];

                            if ( !control.AlwaysOnTop )
                                break;

                            if ( control.Visible &&
                                 control.Locked == false &&
                                 control.Area.Contains( mouse.Position ) )
                            {
                                foundBetterFit = true;
                                mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );

                                controlThatHasMouse.HasMouse = false;

                                controlThatHasMouse = control;
                                controlThatHasMouse.HasMouse = true;
                                controlThatHasMouse.OnMouseEnter( mouse );
                            }
                        }
                    }

                    if ( !foundBetterFit )
                    {
                        mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                        controlThatHasMouse.OnMouseMove( mouse );
                    }

                    return;
                }

                // Testing if the mouse is down here so that 
                // the controls can receive the movement even when
                // the mouse is not actually on the control.
                if ( mouseIsDown || controlThatHasMouse.Area.Contains( mouse.Position ) )
                {
                    mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                    controlThatHasMouse.OnMouseMove( mouse );
                    return;
                }
                controlThatHasMouse.OnMouseLeave( mouse );
                controlThatHasMouse = null;
            }

            // Find the control that the mouse is on.
            for ( var i = 0; i < controls.Count; i++ )
            {
                var control = controls[ i ];

                if ( control.Visible &&
                     control.Locked == false &&
                     control.Area.Contains( mouse.Position ) )
                {
                    controlThatHasMouse = control;
                    mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                    controlThatHasMouse.OnMouseEnter( mouse );
                    break;
                }
            }
        }

        // OnWMMOUSELEAVE
        //______________________________________________________________________________________

        void OnWMMOUSELEAVE( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );
            mouseTracking = false;

            if ( locked || OnMouseLeave( mouse ) )
                return;

            if ( controlThatHasMouse != null &&
                 controlThatHasMouse.Locked == false &&
                 controlThatHasMouse.HasMouse == false )
            {
                controlThatHasMouse.OnMouseLeave( mouse );
                controlThatHasMouse = null;
            }
        }

        // OnWMMOUSEDBLCLK
        //______________________________________________________________________________________

        void OnWMMOUSEDBLCLK( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );

            if ( locked || OnMouseDoubleClick( mouse ) )
                return;

            if ( controlThatHasMouse != null )
            {
                mouse.Position = controlThatHasMouse.FromWindowToControl( mouse.Position );
                controlThatHasMouse.OnMouseDoubleClick( mouse );
            }
        }

        // OnWMMOUSEWHEEL
        //______________________________________________________________________________________

        void OnWMMOUSEWHEEL( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouse = new Mouse( msg, wParam, lParam );

            if ( locked || OnMouseWheel( mouse ) )
                return;

            if ( controlThatHasMouse != null )
            {
                mouse.Position = controlThatHasMouse.FromScreenToControl( mouse.Position );
                controlThatHasMouse.OnMouseWheel( mouse );
            }
        }

        // OnWMKEYUP
        //______________________________________________________________________________________

        void OnWMKEYUP( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            if ( locked )
                return;

            var key = Keyboard.MessageToKey( wParam, lParam );

            if ( key == Key.None )
                return;

            if ( (uint)wParam == Windows.VK_SHIFT )
            {
                // Release both Shift keys on Shift up event, as only one event is sent even if both keys are released.
                if ( !OnKeyUp( Key.ShiftLeft ) && !OnKeyUp( Key.ShiftRight ) )
                {
                    controlThatHasKeyboard?.OnKeyUp( Key.ShiftLeft );
                    controlThatHasKeyboard?.OnKeyUp( Key.ShiftRight );
                }
            }
            else if ( (uint)wParam == Windows.VK_SNAPSHOT )
            {
                // Key down is not reported for the print screen key.
                if ( !OnKeyDown( Key.PrintScreen ) && !OnKeyUp( Key.PrintScreen ) )
                {
                    controlThatHasKeyboard?.OnKeyDown( Key.PrintScreen );
                    controlThatHasKeyboard?.OnKeyUp( Key.PrintScreen );
                }
            }
            else if ( !OnKeyUp( key ) )
            {
                controlThatHasKeyboard?.OnKeyUp( key );
            }
        }

        // OnWMKEYDOWN
        //______________________________________________________________________________________

        void OnWMKEYDOWN( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            if ( locked )
                return;

            var key = Keyboard.MessageToKey( wParam, lParam );

            if ( key == Key.None )
                return;

            if ( !OnKeyDown( key ) )
            {
                controlThatHasKeyboard?.OnKeyDown( key );
            }
        }

        // OnWMCHAR
        //______________________________________________________________________________________

        void OnWMCHAR( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            if ( locked )
                return;

            var character = (char)(int)wParam;

            if ( !OnCharacter( character ) )
            {
                controlThatHasKeyboard?.OnCharacter( character );
            }
        }

        // OnWMGETMINMAXINFO
        //______________________________________________________________________________________

        void OnWMGETMINMAXINFO( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var monitorInfo = new MonitorInfo( hWnd );
            var minMaxInfo = (Windows.MINMAXINFO)Marshal.PtrToStructure( lParam, typeof( Windows.MINMAXINFO ) );
            minMaxInfo.ptMaxPosition.x = 0;
            minMaxInfo.ptMaxPosition.y = 0;
            minMaxInfo.ptMaxSize.x = (int)(monitorInfo.WorkArea.Right - monitorInfo.WorkArea.Left);
            minMaxInfo.ptMaxSize.y = (int)(monitorInfo.WorkArea.Bottom - monitorInfo.WorkArea.Top);
            //minMaxInfo.ptMaxTrackSize = minMaxInfo.ptMaxSize;
            //minMaxInfo.ptMaxTrackSize.x = (int)(monitorInfo.WorkArea.Width) - 10;
            //minMaxInfo.ptMaxTrackSize.y = (int)(monitorInfo.WorkArea.Height) - 10;
            minMaxInfo.ptMinTrackSize.x = (int)settings.MinimumSize.Width;
            minMaxInfo.ptMinTrackSize.y = (int)settings.MinimumSize.Height;

            // Need to set the lParam pointer to our new data.
            Marshal.StructureToPtr( minMaxInfo, lParam, false );
        }

        // OnWMPOSCHANGING
        //______________________________________________________________________________________

        const int SnapThreshold = 10;
        static bool SnapTest( int position, float target )
        {
            return position > target - SnapThreshold && position < target + SnapThreshold;
        }

        void OnWMPOSCHANGING( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            // Find the current window state.
            Windows.WINDOWPLACEMENT wp = Windows.WINDOWPLACEMENT.Make();
            Windows.GetWindowPlacement( hWnd, ref wp );

            switch ( wp.showCmd )
            {
                case Windows.SW_NORMAL:
                case Windows.SW_RESTORE:
                case Windows.SW_SHOW:
                case Windows.SW_SHOWNA:
                case Windows.SW_SHOWNOACTIVATE:
                    windowState = WindowState.Normal;
                    break;
                case Windows.SW_SHOWMINIMIZED:
                case Windows.SW_MINIMIZE:
                case Windows.SW_SHOWMINNOACTIVE:
                    windowState = WindowState.Minimized;
                    break;
                case Windows.SW_SHOWMAXIMIZED:
                    windowState = WindowState.Maximized;
                    break;
            }

            // Get the monitor size info and marshal the position info from lParam.
            var monitorInfo = new MonitorInfo( hWnd );
            var posInfo = (Windows.WINDOWPOS)Marshal.PtrToStructure( lParam, typeof( Windows.WINDOWPOS ) );

            // Get the new window size, the real resize work is done in the property setter.
            if ( (posInfo.flags & Windows.SWP_NOSIZE) == 0 )
            {
                Area = Rectangle.FromXYWH( posInfo.x, posInfo.y, posInfo.cx, posInfo.cy );
            }

            // Perform snap testing on the new window position and change accordingly.
            if ( ((posInfo.flags & Windows.SWP_NOMOVE) == 0) && windowState == WindowState.Normal )
            {
                // Check if we actually changed the window position to avoid marshaling penalty when not needed.
                var snapped = false;

                // Horizontal testing.
                if ( SnapTest( posInfo.x, monitorInfo.WorkArea.Left ) )
                {
                    snapped = true;
                    posInfo.x = (int)monitorInfo.WorkArea.Left;
                }
                else if ( SnapTest( posInfo.x + posInfo.cx, monitorInfo.WorkArea.Right ) )
                {
                    snapped = true;
                    posInfo.x = (int)(monitorInfo.WorkArea.Right - posInfo.cx);
                }

                // Vertical testing.
                if ( SnapTest( posInfo.y, monitorInfo.WorkArea.Top ) )
                {
                    snapped = true;
                    posInfo.y = (int)monitorInfo.WorkArea.Top;
                }
                else if ( SnapTest( posInfo.y + posInfo.cy, monitorInfo.WorkArea.Bottom ) )
                {
                    snapped = true;
                    posInfo.y = (int)(monitorInfo.WorkArea.Bottom - posInfo.cy);
                }

                // Marshal the data back.
                if ( snapped )
                    Marshal.StructureToPtr( posInfo, lParam, false );
            }
        }

        // OnWMNCHITTEST
        //______________________________________________________________________________________

        IntPtr OnWMNCHITTEST( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var mouseOnScreen = new Point( Windows.GET_X_LPARAM( lParam ), Windows.GET_Y_LPARAM( lParam ) );
            var mouseOnWindow = FromScreenToWindow( mouseOnScreen );

            var result = Windows.HTCLIENT;
            var margings = settings.Margings;

            if ( settings.NoTitle || locked )
                return new IntPtr( result );

            var windowCornerSize = 5;

            if ( mouseOnWindow.X <= margings.Left )
            {
                if ( mouseOnWindow.Y <= windowCornerSize )
                    result = Windows.HTTOPLEFT;
                else if ( mouseOnWindow.Y >= settings.Area.Height - margings.Bottom )
                    result = Windows.HTBOTTOMLEFT;
                else
                    result = Windows.HTLEFT;
            }
            else if ( mouseOnWindow.X >= settings.Area.Width - margings.Right )
            {
                if ( mouseOnWindow.Y < windowCornerSize )
                    result = Windows.HTTOPRIGHT;
                else if ( mouseOnWindow.Y >= settings.Area.Height - windowCornerSize )
                    result = Windows.HTBOTTOMRIGHT;
                else
                    result = Windows.HTRIGHT;
            }
            else if ( mouseOnWindow.Y <= margings.Top )
            {
                if ( mouseOnWindow.Y <= windowCornerSize )
                    result = Windows.HTTOP;
                else
                {
                    if ( !OnHitTest( mouseOnWindow ) )
                    {
                        result = Windows.HTCAPTION;
                    }
                }
            }
            else if ( mouseOnWindow.Y >= settings.Area.Height - windowCornerSize )
            {
                result = Windows.HTBOTTOM;
            }
            else
            {
                result = Windows.HTCLIENT;
            }

            return new IntPtr( result );
        }

        // OnWMCLOSE
        //______________________________________________________________________________________

        void OnWMCLOSE( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            if ( OnClosing() )
            {
                Windows.DestroyWindow( hWnd );
            }
        }

        // OnWMDESTROY
        //______________________________________________________________________________________

        void OnWMDESTROY( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            Release();
            manager.GoingAway( this );
        }

        IntPtr WndProc( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            //try
            //{
            return WndProcInner( hWnd, msg, wParam, lParam );
            /*}
            catch ( Exception e )
            {
                manager.UnhandledException( e );
            }*/

            //return Windows.DefWindowProc( hWnd, msg, wParam, lParam );
        }

        IntPtr WndProcInner( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            switch ( msg )
            {
                case Windows.WM_NCCALCSIZE:
                    if ( (int)wParam == 1 )
                    {
                        var monitorInfo = new MonitorInfo( hWnd );
                        var calcSizeParams = (Windows.NCCALCSIZE_PARAMS)Marshal.PtrToStructure( lParam, typeof( Windows.NCCALCSIZE_PARAMS ) );
                        if ( calcSizeParams.rgrc[ 0 ].left < monitorInfo.WorkArea.Left )
                        {
                            calcSizeParams.rgrc[ 0 ] = new Windows.RECT
                            {
                                left = (int)monitorInfo.WorkArea.Left,
                                top = (int)monitorInfo.WorkArea.Top,
                                bottom = (int)monitorInfo.WorkArea.Bottom,
                                right = (int)monitorInfo.WorkArea.Right
                            };

                            Marshal.StructureToPtr( calcSizeParams, lParam, false );
                        }
                        //return IntPtr.Zero;
                        return new IntPtr( Windows.WVR_ALIGNLEFT | Windows.WVR_ALIGNTOP );
                    }

                    return IntPtr.Zero;

                case Windows.WM_NCHITTEST:
                    return OnWMNCHITTEST( hWnd, msg, wParam, lParam );

                case Windows.WM_LBUTTONUP:
                case Windows.WM_RBUTTONUP:
                case Windows.WM_MBUTTONUP:
                case Windows.WM_XBUTTONUP:
                    OnWMMOUSEUP( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_LBUTTONDOWN:
                case Windows.WM_RBUTTONDOWN:
                case Windows.WM_MBUTTONDOWN:
                case Windows.WM_XBUTTONDOWN:
                    OnWMMOUSEDOWN( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_LBUTTONDBLCLK:
                case Windows.WM_RBUTTONDBLCLK:
                case Windows.WM_MBUTTONDBLCLK:
                case Windows.WM_XBUTTONDBLCLK:
                    OnWMMOUSEDBLCLK( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_MOUSEWHEEL:
                case Windows.WM_MOUSEHWHEEL:
                    OnWMMOUSEWHEEL( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_MOUSEMOVE:
                    OnWMMOUSEMOVE( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_MOUSEHOVER:
                    OnWMMOUSEHOVER( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_MOUSELEAVE:
                    OnWMMOUSELEAVE( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_KEYDOWN:
                case Windows.WM_SYSKEYDOWN:
                    OnWMKEYDOWN( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_KEYUP:
                case Windows.WM_SYSKEYUP:
                    OnWMKEYUP( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_CHAR:
                    OnWMCHAR( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_GETMINMAXINFO:
                    OnWMGETMINMAXINFO( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_WINDOWPOSCHANGING:
                    OnWMPOSCHANGING( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_DWMCOLORIZATIONCOLORCHANGED:
                    Update();
                    return IntPtr.Zero;

                case Windows.WM_PAINT:
                case Windows.WM_DISPLAYCHANGE:
                    Draw();
                    Windows.ValidateRect( hWnd, IntPtr.Zero );
                    return IntPtr.Zero;

                case Windows.WM_CLOSE:
                    OnWMCLOSE( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_DESTROY:
                    OnWMDESTROY( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;

                case Windows.WM_DROPFILES:
                    OnWMDROPFILES( hWnd, msg, wParam, lParam );
                    return IntPtr.Zero;
            }

            return Windows.DefWindowProc( hWnd, msg, wParam, lParam );
        }
    }
}
