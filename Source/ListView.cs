﻿using System;
using System.Collections.Generic;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Represents a method to be invoked when a list view is scrolled.
    /// </summary>
    /// <param name="viewBegin">The first index within view.</param>
    /// <param name="viewEnd">The last index within view.</param>
    public delegate void ListViewScrollDelegate( int viewBegin, int viewEnd );

    /// <summary>
    /// Defines the selection modes for a <see cref="ListView{T}"/>.
    /// </summary>
    public enum ListViewSelectMode
    {
        /// <summary>
        /// Nothing can be selected.
        /// </summary>
        None,
        /// <summary>
        /// Only one item can be selected at a time.
        /// </summary>
        Single,
        /// <summary>
        /// Only one item can be selected and it is immediately activated.
        /// </summary>
        SingleActivation,
        /// <summary>
        /// Multiple items can be selected.
        /// </summary>
        Multi
    }

    /// <summary>
    /// Common base for list view type controls.
    /// </summary>
    /// <typeparam name="T">Type of item contained in the list.</typeparam>
    public abstract class ListView<T> : Control
    {
        /// <summary>
        /// Represents a method to be invoked when an item gets activated.
        /// </summary>
        /// <param name="item">Item that was activated.</param>
        public delegate void OnItemActivatedDelegate( T item );

        HashSet<int> selectedItems = new HashSet<int>();

        struct ListViewItem
        {
            public readonly int Index;
            public readonly bool Selected;

            public ListViewItem( int i, bool s )
            {
                Index = i;
                Selected = s;
            }
        }

        enum ScrollMode
        {
            CenterTo,
            CenterToLazy,
            BringToView,
            Scrollbar,
            Forward,
            Backward
        }

        /// <summary>
        /// Occurs when an item is activated.
        /// </summary>
        public event OnItemActivatedDelegate OnItemActivated;

        /// <summary>
        /// Represents an invalid index.
        /// </summary>
        protected const int InvalidIndex = -1;

        const float defaultItemHeight = 36;

        int viewIndexBegin;
        int viewIndexEnd;
        int viewIndexRange;

        bool searchIsEnabled;
        string searchString;
        bool mouseIsDown;
        bool mouseIsHovering;
        bool shiftActivated;
        bool keepCentered = true;
        int maxItemWidth;
        float itemWidthMod;

        int indexActive = InvalidIndex;
        int indexSelected = InvalidIndex;
        int indexCursor = InvalidIndex;
        bool keyboardCursor;

        List<ListViewItem> indices;

        bool allowSearch = true;
        bool allowActivation;
        bool allowMoving;
        ListViewSelectMode selectMode = ListViewSelectMode.Multi;

        Trackbar trackbar;

        /// <summary> 
        /// Occurs when the list is scrolled. 
        /// </summary>
        public event ListViewScrollDelegate OnScroll;

        /// <summary> 
        /// Get or set the <see cref="Style.Trackbar"/> for scrolling the list. 
        /// </summary>
        public Trackbar Trackbar
        {
            get { return trackbar; }
            set
            {
                if ( trackbar != value )
                {
                    if ( trackbar != null ) // Disconnect from the old scrollbar.	
                        trackbar.Scroll -= OnTrackbarSlide;

                    trackbar = value;

                    if ( trackbar != null ) // Connect to the new scrollbar.
                        trackbar.Scroll += OnTrackbarSlide;

                    TrackbarUpdate();
                }
            }
        }

        /// <summary>
        /// Specifies the maximum width of items in the list.
        /// </summary>
        public virtual int MaxItemWidth
        {
            get { return maxItemWidth; }
            set
            {
                maxItemWidth = value;
                Update();
            }
        }

        /// <summary> 
        /// Get or set the <see cref="ListViewSelectMode"/> for the list. 
        /// </summary>
        public virtual ListViewSelectMode SelectionMode
        {
            get { return selectMode; }
            set { selectMode = value; }
        }

        /// <summary> 
        /// Get or set whether or not the list can be searched by the user.
        /// </summary>
        public virtual bool AllowSearch
        {
            get { return allowSearch; }
            set { allowSearch = value; }
        }

        /// <summary> 
        /// Get or set whether or not the list can be rearranged by the user.
        /// </summary>
        public virtual bool AllowMoving
        {
            get { return allowMoving; }
            set { allowMoving = value; }
        }

        /// <summary>
        /// Get or set whether or not it is possible to activate items in the list.
        /// </summary>
        public virtual bool AllowActivation
        {
            get { return allowActivation; }
            set { allowActivation = value; }
        }

        /// <summary>
        /// Get or set the height of the items in the list.
        /// </summary>
        protected virtual float ActualItemHeight
        {
            get { return defaultItemHeight; }
        }

        /// <summary>
        /// Return the item or an invalid item value with the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        protected abstract T Get( int index );
        /// <summary>
        /// Return the number of items in the list.
        /// </summary>
        protected abstract int GetCount();
        /// <summary>
        /// Draw the specified item.
        /// </summary>
        /// <param name="context">The drawing <see cref="Context"/>.</param>
        /// <param name="item">The item.</param>
        /// <param name="area">The <see cref="Rectangle"/> that specifies the item area.</param>
        /// <param name="selected">Indicates whether or not the item is selected.</param>
        protected abstract void OnDrawItem( Context context, T item, Rectangle area, bool selected, bool active );

        /// <summary>
        /// Indicates whether or the the item with the specified index matches in some way to the search string.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="search">The search string.</param>
        protected virtual bool Match( int index, string search )
        {
            return false;
        }

        /// <summary>
        /// Remove a list of items from the list.
        /// </summary>
        /// <param name="items"></param>
        protected virtual void Remove( List<T> items )
        {
        }

        /// <summary>
        /// Get the index of the active item. Returns <see cref="InvalidIndex"/> if no active item.
        /// </summary>
        public virtual int GetActiveIndex()
        {
            return indexActive;
        }

        /// <summary>
        /// Set the index of the active item. <see cref="InvalidIndex"/> deactivates the active item.
        /// </summary>
        protected virtual void SetActiveIndex( int index )
        {
            indexActive = index;
        }

        /// <summary>
        /// Swap two indexes with each other.
        /// </summary>
        /// <param name="x">First.</param>
        /// <param name="y">Second.</param>
        protected virtual void Swap( int x, int y )
        {
        }

        /// <summary>
        /// Indicates whether or not the specified index is selected.
        /// </summary>
        /// <param name="index">The index.</param>
        protected bool IsSelected( int index )
        {
            return indices[ index ].Selected;
        }

        /// <summary> 
        /// Create a new instance of <see cref="ListView{T}"/>. 
        /// </summary>
        /// <param name="window">The containing <see cref="Window"/>.</param>
        public ListView( Window window )
            : base( window )
        {
            indices = new List<ListViewItem>();
        }

        /// <summary> 
        /// Get the current range of indexes displayed. 
        /// </summary>
        /// <param name="begin">The first index displayed.</param>
        /// <param name="end">The last index displayed.</param>
        public void GetCurrentViewRange( out int begin, out int end )
        {
            begin = viewIndexBegin;
            end = viewIndexEnd;
        }

        static int Clamp( int value, int min, int max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        float CalculateItemHeight()
        {
            /*var ith = ActualItemHeight;

            var rem = (float)Math.IEEERemainder( Area.Height, ith );

            if ( rem <= ith )
            {
                ith += rem / (int)(Area.Height / ith);
            }*/

            return ActualItemHeight;
        }

        /// <summary>
        /// Called when the list needs to be updated.
        /// </summary>
        /// <param name="context">The <see cref="Context"/>.</param>
        protected internal override void OnUpdate( Context context )
        {
            var size = Area.Dimensions;
            var ith = CalculateItemHeight();

            if ( searchIsEnabled )
            {
                viewIndexRange = (int)Math.Floor( size.Height / ith );
                viewIndexRange -= 1;
            }
            else
            {
                viewIndexRange = (int)Math.Floor( size.Height / ith );
            }

            viewIndexRange = Clamp( viewIndexRange, 0, indices.Count );
            viewIndexBegin = Clamp( viewIndexBegin, 0, indices.Count - viewIndexRange );
            viewIndexEnd = viewIndexBegin + viewIndexRange;

            TrackbarMode( viewIndexRange != indices.Count );

            if ( keepCentered )
            {
                CenterToActive();
            }

            if ( MaxItemWidth > 0 && size.Width - MaxItemWidth > 0 )
            {
                itemWidthMod = (size.Width - MaxItemWidth) / 2f;
            }
        }

        /// <summary>
        /// Called when the list needs to be drawn.
        /// </summary>
        /// <param name="context">The <see cref="Context"/>.</param>
        protected internal override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );
            var itemHeight = CalculateItemHeight();

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2 );

            var itemClip = new Rectangle( clip.Left + itemWidthMod, clip.Top, clip.Right - itemWidthMod, clip.Top + itemHeight );

            if ( searchIsEnabled )
            {
                SharedBrush.Color = Color.FromAColor( 0.05f, Color.GhostWhite );
                context.FillRectangle( itemClip, SharedBrush );
                context.FillRectangle( itemClip, SharedStyle.ForegroundHover );

                SharedBrush.Color = Color.LightSkyBlue;
                TextStyle.Alignment = TextStyleAlignment.LeftCenter;
                context.DrawText( "Search: " + searchString, TextStyle, itemClip, SharedBrush );

                itemClip = itemClip.Move( 0, itemHeight );
            }

            if ( viewIndexEnd - viewIndexBegin <= 0 )
            {
                SharedBrush.Color = Color.GhostWhite;
                TextStyle.Alignment = TextStyleAlignment.CenterCenter;
                context.DrawText( "There appears to be nothing here.", TextStyle, clip, SharedBrush );
                return;
            }

            var actI = GetActiveIndex();

            for ( var i = viewIndexBegin; i < viewIndexEnd; i++ )
            {
                var item = indices[ i ];

                SharedStyle.Align( itemClip );
                SharedBrush.Color = Color.Black;
                context.DrawRectangle( itemClip, SharedBrush, 2 );

                if ( item.Selected )
                {
                    SharedBrush.Color = Color.FromAColor( 0.05f, Color.GhostWhite );
                    context.FillRectangle( itemClip, SharedBrush );

                    if ( indexCursor == i )
                        context.FillRectangle( itemClip, SharedStyle.ForegroundHover );
                }
                else if ( indexCursor == i )
                    context.FillRectangle( itemClip, SharedStyle.ForegroundHover );
                else
                    context.FillRectangle( itemClip, SharedStyle.Foreground );

                OnDrawItem( context, Get( item.Index ), itemClip, item.Selected, i == actI );

                itemClip = itemClip.Move( 0, itemHeight );
            }
        }

        /// <summary> 
        /// Get the currently selected items. 
        /// </summary>
        /// <returns>The selected items.</returns>
        public List<T> GetSelectedItems()
        {
            var result = new List<T>();

            for ( var i = 0; i < indices.Count; i++ )
            {
                if ( indices[ i ].Selected )
                {
                    result.Add( Get( indices[ i ].Index ) );
                }
            }

            return result;
        }

        void OnTrackbarSlide( int value )
        {
            Scroll( ScrollMode.Scrollbar, value );
            Refresh();
        }

        void TrackbarMode( bool value )
        {
            if ( trackbar == null )
                return;

            if ( value )
            {
                trackbar.ThumbVisible = true;
                trackbar.Set( viewIndexBegin, 0, indices.Count - viewIndexRange );
            }
            else
            {
                trackbar.ThumbVisible = false;
                trackbar.Set( 0, 0, 0 );
            }
        }

        void TrackbarUpdate()
        {
            trackbar?.Set( viewIndexBegin, 0, indices.Count - viewIndexRange );
        }

        void Scroll( ScrollMode mode, int parameter = 1 )
        {
            var updateScrollbar = true;
            keepCentered = false;

            switch ( mode )
            {
                case ScrollMode.CenterTo:
                    if ( parameter == InvalidIndex )
                        return;
                    keepCentered = true;
                    viewIndexBegin = Clamp( parameter - viewIndexRange / 2, 0, indices.Count - viewIndexRange );
                    break;

                case ScrollMode.CenterToLazy:
                    if ( parameter == InvalidIndex )
                        return;
                    if ( ItemWithinView( parameter ) )
                    {
                        if ( parameter <= viewIndexBegin + 2 ||
                             parameter >= viewIndexEnd - 2 )
                        {
                            Scroll( ScrollMode.CenterTo, parameter );
                        }
                    }
                    else
                    {
                        keepCentered = true;
                        viewIndexBegin = Clamp( parameter - viewIndexRange / 2, 0, indices.Count - viewIndexRange );
                    }
                    break;

                case ScrollMode.Scrollbar:
                    if ( viewIndexBegin != parameter )
                    {
                        viewIndexBegin = Clamp( parameter, 0, indices.Count - viewIndexRange );
                        updateScrollbar = false;
                    }
                    break;

                case ScrollMode.BringToView:
                    if ( parameter == InvalidIndex )
                        return;

                    if ( parameter < viewIndexBegin )
                        viewIndexBegin = parameter;
                    else if ( parameter >= viewIndexEnd )
                        viewIndexBegin = Clamp( viewIndexBegin + 1, 0, indices.Count - viewIndexRange );
                    break;

                case ScrollMode.Backward:
                    if ( viewIndexBegin > 0 )
                        viewIndexBegin -= parameter;
                    break;

                case ScrollMode.Forward:
                    if ( viewIndexBegin < indices.Count - viewIndexRange )
                        viewIndexBegin += parameter;
                    break;
            }

            viewIndexEnd = viewIndexBegin + viewIndexRange;

            if ( updateScrollbar )
                TrackbarUpdate();

            OnScroll?.Invoke( viewIndexBegin, viewIndexEnd );
        }

        /// <summary>
        /// Clear all selections.
        /// </summary>
        public void SelectClear()
        {
            indexSelected = InvalidIndex;

            for ( var i = 0; i < indices.Count; i++ )
            {
                indices[ i ] = new ListViewItem( indices[ i ].Index, false );
            }
        }

        void SelectRange( int start, int end )
        {
            for ( var i = start; i <= end; ++i )
            {
                indices[ i ] = new ListViewItem( indices[ i ].Index, true );
            }
        }

        /// <summary>
        /// Select all items in the list.
        /// </summary>
        public void SelectAll()
        {
            Select( 0 );
            Select( indices.Count - 1, false, true );
            Refresh();
        }

        void Select( int target, bool ctrlMode = false, bool shiftMode = false )
        {
            if ( !ItemWithinBounds( target ) || SelectionMode == ListViewSelectMode.None ) // invalid target index causes the selection to be cleared.
            {
                SelectClear();
            }
            else if ( shiftMode && SelectionMode == ListViewSelectMode.Multi ) // shift-mode selects ranges of items starting from the pivot-Point ( indexSelected ) to the target ( target_idx ), clearing the old selection.
            {
                int start;
                int end;

                if ( indexSelected == InvalidIndex )
                    indexSelected = 0;

                if ( !ctrlMode ) // shift-mode with ctrl-mode selects ranges of items like shift-mode, but does not clear the old selection.
                {
                    // store the current selected fm_item, clear the selection and restore the selected fm_item.
                    start = indexSelected;
                    SelectClear();
                    indexSelected = start;
                }

                // Set the selection range.
                if ( target > indexSelected )
                {
                    start = indexSelected;
                    end = target;
                }
                else
                {
                    start = target;
                    end = indexSelected;
                }

                SelectRange( start, end );
            }
            else if ( ctrlMode && SelectionMode == ListViewSelectMode.Multi ) // ctrl-mode toggles the selection status of the target fm_item.
            {
                var item = indices[ target ];
                indices[ target ] = new ListViewItem( item.Index, !item.Selected );

                if ( item.Selected )
                    indexSelected = target;
            }
            else // single mode selection clears the current selection and selects the target fm_item.
            {
                SelectClear();

                var item = indices[ target ];
                indices[ target ] = new ListViewItem( item.Index, true );

                indexSelected = target;
            }

            if ( SelectionMode == ListViewSelectMode.SingleActivation )
            {
                if ( indexSelected != InvalidIndex )
                {
                    ActivateCurrentItem();
                }
            }
        }

        bool ItemWithinBounds( int item )
        {
            return item >= 0 && item < indices.Count;
        }

        bool ItemWithinView( int item )
        {
            return item >= viewIndexBegin && item <= viewIndexEnd;
        }

        /// <summary>
        /// Get the index at the specified location.
        /// </summary>
        /// <param name="point">The location.</param>
        /// <returns>The index.</returns>
        protected int GetIndexAt( Point point )
        {
            return CoordinateToIndex( point );
        }

        /// <summary>
        /// Get the item at the specified location.
        /// </summary>
        /// <param name="point">The location.</param>
        /// <returns>The item.</returns>
        public T GetItemAt( Point point )
        {
            return Get( CoordinateToIndex( point ) );
        }

        int CoordinateToIndex( Point point )
        {
            if ( itemWidthMod > 0 )
            {
                if ( point.X < itemWidthMod )
                    return InvalidIndex;
                if ( point.X > itemWidthMod + MaxItemWidth )
                    return InvalidIndex;
            }

            var result_idx = InvalidIndex;
            var y = (int)point.Y;
            var yShift = searchIsEnabled ? (int)CalculateItemHeight() : 0;

            if ( y >= yShift )
            {
                result_idx = viewIndexBegin + (int)((y - yShift) / CalculateItemHeight());

                if ( result_idx >= indices.Count )
                    result_idx = InvalidIndex;
            }

            return result_idx;
        }

        void ShiftSelected( int direction )
        {
            int mod;
            int start;
            int end;

            if ( direction < 0 )
            {
                mod = 1;
                start = 0;
                end = indices.Count;
            }
            else
            {
                mod = -1;
                start = indices.Count - 1;
                end = -1;
            }

            for ( var i = start; i != end; i += mod )
            {
                if ( indices[ i ].Selected )
                {
                    if ( i - mod == end )
                        break;

                    var x = i - mod;
                    var y = i;

                    Swap( x, y );

                    //var temp = indices[ x ];
                    indices[ x ] = new ListViewItem( x, indices[ y ].Selected );
                    indices[ y ] = new ListViewItem( y, false );

                }
            }

            Reload();
        }

        void ActivateCurrentItem()
        {
            var indexToActivate = InvalidIndex;

            if ( AllowActivation )
            {
                if ( keyboardCursor )
                    indexToActivate = indexCursor;
                else
                    indexToActivate = indexSelected;
            }

            Select( -1 );

            if ( indexToActivate == InvalidIndex )
                return;

            SetActiveIndex( indices[ indexToActivate ].Index );

            if ( OnItemActivated != null )
            {
                var activeIndex = GetActiveIndex();

                if ( activeIndex != InvalidIndex )
                {
                    OnItemActivated( Get( activeIndex ) );
                }
            }

            if ( searchIsEnabled )
            {
                SearchMode( false );
            }

            Refresh();
        }

        /// <summary>
        /// Update the list.
        /// </summary>
        protected void Reload()
        {
            if ( searchIsEnabled )
            {
                SearchUpdate();
            }
            else
            {
                var count = GetCount();

                if ( indices.Count != count )
                {
                    if ( indices.Capacity < count )
                        indices.Capacity = count;

                    indices.Clear();

                    for ( var i = 0; i < count; i++ )
                        indices.Add( new ListViewItem( i, false ) );
                }
            }

            CenterToActive();
            Update();
        }

        void SearchUpdate()
        {
            indices.Clear();

            indexSelected = InvalidIndex;
            indexCursor = InvalidIndex;

            var count = GetCount();

            for ( var i = 0; i < count; i++ )
            {
                if ( Match( i, searchString ) )
                {
                    indices.Add( new ListViewItem( i, false ) );
                }
            }

            Update();
        }

        void SearchMode( bool enable )
        {
            if ( searchIsEnabled == enable )
                return;

            SelectClear();
            searchIsEnabled = enable;
            searchString = string.Empty;

            if ( enable )
            {
                KeyboardCursorMove( 0 );
                SearchUpdate();
            }
            else
            {
                Reload();
            }
        }

        /// <summary>
        /// Centers the view to the active iten.
        /// </summary>
        /// <param name="careAboutUser">Whether or not we care about user input.</param>
        protected void CenterToActive( bool careAboutUser = true )
        {
            //
            // Don't move the view automatically if the user has recently done something.
            //
            if ( !careAboutUser || !mouseIsHovering )
            {
                Scroll( ScrollMode.CenterTo, GetActiveIndex() );
                Refresh();
            }
        }

        /// <summary>
        /// Lazily centers the view to the active item.
        /// </summary>
        /// <param name="careAboutUser">Whether or not we care about user input.</param>
        protected void CenterToActiveLazy( bool careAboutUser = true )
        {
            //
            // Don't move the view automatically if the user has recently done something.
            //
            if ( !careAboutUser || !mouseIsHovering )
            {
                Scroll( ScrollMode.CenterToLazy, GetActiveIndex() );
                Refresh();
            }
        }

        protected void RemoveAt( int index )
        {
            if ( !ItemWithinBounds( index ) )
                throw new ArgumentOutOfRangeException( nameof( index ) );

            if ( searchIsEnabled )
            {
                for ( var i = 0; i < indices.Count; i++ )
                {
                    if ( indices[ i ].Index == index )
                    {
                        indices.RemoveAt( i );
                        break;
                    }
                }
            }
            else
            {
                indices.RemoveAt( index );

                for ( var i = 0; i < indices.Count; i++ )
                {
                    var item = indices[ i ];
                    indices[ i ] = new ListViewItem( i, item.Selected );
                }
            }
        }

        void DeleteSelected()
        {
            var toRemove = new List<T>();

            for ( var i = 0; i < indices.Count; i++ )
            {
                if ( indices[ i ].Selected )
                {
                    toRemove.Add( Get( indices[ i ].Index ) );
                }
            }

            Remove( toRemove );
        }

        // ________________ Keyboard ___________________________________________________________

        /// <summary>
        /// Called when the list receives a characted.
        /// </summary>
        /// <param name="c">The character.</param>
        protected internal override void OnCharacter( char c )
        {
            if ( searchIsEnabled && !char.IsControl( c ) )
            {
                searchString += c;
                SearchUpdate();
            }
            else if ( searchIsEnabled && c == '\b' )
            {
                if ( searchString.Length > 0 )
                {
                    searchString = searchString.Substring( 0, searchString.Length - 1 );
                    SearchUpdate();
                }
                else
                    SearchMode( false );
            }
            else if ( c == 'f' || c == 'F' )
            {
                SearchMode( true );
            }

            base.OnCharacter( c );
        }

        /// <summary>
        /// Called when the list receives a key up event.
        /// </summary>
        /// <param name="key">The key.</param>
        protected internal override void OnKeyUp( Key key )
        {
            switch ( key )
            {
                case Key.AltLeft:
                    if ( shiftActivated )
                    {
                        shiftActivated = false;
                        Refresh();
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when the list receives a key down event.
        /// </summary>
        /// <param name="key">The key.</param>
        protected internal override void OnKeyDown( Key key )
        {
            switch ( key )
            {
                case Key.AltLeft:
                    if ( AllowMoving && Keyboard.IsDown( Key.AltLeft ) )
                    {
                        shiftActivated = true;
                        Refresh();
                    }
                    break;

                case Key.J:
                    if ( searchIsEnabled && Keyboard.IsDown( Key.Ctrl ) )
                    {
                        int indexToJump;

                        if ( keyboardCursor )
                            indexToJump = indexCursor;
                        else
                            indexToJump = indexSelected;

                        if ( indexToJump == InvalidIndex )
                            return;

                        indexToJump = indices[ indexToJump ].Index;

                        if ( searchIsEnabled )
                        {
                            SearchMode( false );
                        }

                        Scroll( ScrollMode.CenterTo, indexToJump );
                    }
                    break;

                case Key.Space:
                    CenterToActive( false );
                    break;

                case Key.A:
                    if ( Keyboard.IsDown( Key.Ctrl ) )
                        SelectAll();
                    break;

                case Key.Delete:
                    DeleteSelected();
                    break;

                case Key.Escape:
                    if ( searchIsEnabled )
                    {
                        if ( indexSelected != InvalidIndex )
                        {
                            SelectClear();
                            Refresh();
                        }
                        else
                            SearchMode( false );
                    }
                    else
                    {
                        if ( indexSelected != InvalidIndex )
                        {
                            SelectClear();
                            Refresh();
                        }
                    }
                    break;

                case Key.Enter:
                    ActivateCurrentItem();
                    break;

                case Key.ArrowUp:
                    KeyboardCursorMove( -1 );
                    break;

                case Key.ArrowDown:
                    KeyboardCursorMove( 1 );
                    break;
            }
        }

        void KeyboardCursorMove( int direction )
        {
            keyboardCursor = true;

            if ( indexSelected == InvalidIndex )
            {
                if ( searchIsEnabled )
                {
                    if ( ItemWithinBounds( 0 ) )
                    {
                        Select( 0 );
                        indexCursor = 0;
                    }
                }
                else
                {
                    var index = GetActiveIndex();
                    Select( index == InvalidIndex ? 0 : index );
                    indexCursor = indexSelected;
                }
            }
            else
            {
                if ( Keyboard.IsDown( Key.ShiftLeft ) )
                {
                    if ( ItemWithinBounds( indexCursor + direction ) )
                    {
                        Select( indexCursor + direction, Keyboard.IsDown( Key.CtrlLeft ), true );
                        indexCursor += direction;
                    }
                }
                else if ( Keyboard.IsDown( Key.AltLeft ) )
                {
                    ShiftSelected( direction );
                }
                else
                {
                    if ( ItemWithinBounds( indexSelected + direction ) )
                    {
                        Select( indexSelected + direction );
                        indexCursor = indexSelected;
                    }
                }
            }

            Scroll( ScrollMode.BringToView, indexCursor );
            Refresh();
        }

        // ________________ Mouse ___________________________________________________________

        protected internal override void OnMouseEnter( Mouse mouse )
        {
            mouseIsHovering = true;
        }

        protected internal override void OnMouseLeave( Mouse mouse )
        {
            mouseIsHovering = false;
            indexCursor = InvalidIndex;
            Refresh();
        }

        protected internal override void OnMouseDoubleClick( Mouse mouse )
        {
            if ( !mouse.IsPressed( MouseButton.Left ) )
                return;

            if ( !Keyboard.IsDown( Key.AltLeft ) )
            {
                keyboardCursor = false;
                ActivateCurrentItem();
            }
        }

        protected internal override void OnMouseMove( Mouse mouse )
        {
            CaptureKeyboard();

            var index = CoordinateToIndex( mouse.Position );

            if ( indexCursor != index )
            {
                if ( mouseIsDown && shiftActivated )
                {
                    ShiftSelected( index - indexCursor );
                }

                keyboardCursor = false;
                indexCursor = index;

                Refresh();
            }
        }

        protected internal override void OnMouseUp( Mouse mouse )
        {
            if ( mouseIsDown == false )
                return;

            mouseIsDown = false;
            Refresh();
        }

        protected internal override void OnMouseDown( Mouse mouse )
        {
            if ( mouse.IsPressed( MouseButton.Left ) || mouse.IsPressed( MouseButton.Right ) )
            {
                mouseIsDown = true;
                keyboardCursor = false;

                if ( mouse.IsPressed( MouseButton.Left ) )
                {
                    if ( shiftActivated == false )
                    {
                        Select( indexCursor, Keyboard.IsDown( Key.Ctrl ), Keyboard.IsDown( Key.Shift ) );
                        Refresh();
                    }
                }
                else if ( indexCursor != InvalidIndex && !indices[ indexCursor ].Selected )
                {
                    Select( indexCursor, Keyboard.IsDown( Key.Ctrl ), Keyboard.IsDown( Key.Shift ) );
                    Refresh();
                }
            }

            base.OnMouseDown( mouse );
        }

        protected internal override void OnMouseWheel( Mouse mouse )
        {
            Scroll( mouse.WheelDelta < 0 ? ScrollMode.Forward : ScrollMode.Backward );

            if ( mouseIsHovering )
            {
                var index = CoordinateToIndex( mouse.Position );

                if ( index != InvalidIndex && indexCursor != index )
                {
                    indexCursor = index;
                }
            }

            Refresh();
        }
    }
}
