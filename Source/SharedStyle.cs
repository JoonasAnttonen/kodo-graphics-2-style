﻿using System;

namespace Kodo.Graphics.Style
{
    /*public class SharedText
    {
        TextStyle bigFormat;
        TextStyle smallFormat;

        public TextStyle Big
        {
            get { return bigFormat; }
        }

        public TextStyle Small
        {
            get { return smallFormat; }
        }

        public void Align( TextAlignment horizontal, TextAlignment vertical )
        {
            bigFormat.HorizontalAlignment = horizontal;
            bigFormat.VerticalAlignment = vertical;

            smallFormat.HorizontalAlignment = horizontal;
            smallFormat.VerticalAlignment = vertical;
        }

        public SharedText()
        {
            bigFormat = new TextStyle( "Montserrat", 12 );
            smallFormat = new TextStyle( "Montserrat", 11 );
        }
    }*/

    public class SharedStyle : IDisposable
    {
        SolidColorBrush background;

        LinearGradientBrush foreground;
        LinearGradientBrush hover;
        LinearGradientBrush press;

        public SolidColorBrush Background
        {
            get { return background; }
        }

        public LinearGradientBrush Foreground
        {
            get { return foreground; }
        }

        public LinearGradientBrush ForegroundHover
        {
            get { return hover; }
        }

        public LinearGradientBrush ForegroundPress
        {
            get { return press; }
        }

        public SharedStyle( Context context )
        {
            background = new SolidColorBrush( context, new Color( 1.0f, 0.1f, 0.15f, 0.2f ) );

            var collStops = new GradientStop[ 2 ];
            collStops[ 0 ] = new GradientStop( 0.0f, Color.FromAColor( 0.2f, new Color( 1.0f, 0.2f, 0.25f, 0.3f ) ) );
            collStops[ 1 ] = new GradientStop( 1.0f, Color.Undefined );

            using ( var coll = new GradientStopCollection( context, collStops ) )
                foreground = new LinearGradientBrush( context, coll, new LinearGradientBrushProperties() );

            collStops[ 0 ] = new GradientStop( 0.0f, Color.FromAColor( 0.2f, new Color( 1.0f, 0.6f, 0.65f, 0.7f ) ) );

            using ( var coll = new GradientStopCollection( context, collStops ) )
                hover = new LinearGradientBrush( context, coll, new LinearGradientBrushProperties() );

            collStops[ 0 ] = new GradientStop( 0.0f, Color.FromAColor( 0.2f, new Color( 1.0f, 0.8f, 0.5f, 0.5f ) ) );

            using ( var coll = new GradientStopCollection( context, collStops ) )
                press = new LinearGradientBrush( context, coll, new LinearGradientBrushProperties() );
        }

        public void Dispose()
        {
            background.Dispose();
            foreground.Dispose();
            hover.Dispose();
            press.Dispose();
        }

        public void Align( Rectangle area )
        {
            foreground.Start = new Point( 0f, area.Top );
            foreground.End = new Point( 0f, area.Bottom );

            hover.Start = new Point( 0f, area.Top );
            hover.End = new Point( 0f, area.Bottom );

            press.Start = new Point( 0f, area.Top );
            press.End = new Point( 0f, area.Bottom );
        }
    }
}
