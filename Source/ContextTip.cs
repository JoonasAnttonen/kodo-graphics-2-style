﻿namespace Kodo.Graphics.Style
{
    public class ContextTip : Control
    {
        string tip;

        public ContextTip( Window window )
            : base( window )
        {
            Locked = true;
        }

        public void Spawn( Point location, string tipText )
        {
            tip = tipText;

            if ( !string.IsNullOrEmpty( tipText ) )
            {
                var textMetrics = TextStyle.GetTextMetrics( tipText );

                var tipsiz = new Size( textMetrics.Width + 10, textMetrics.Height + 10 );
                var tiploc = new Point( location.X, location.Y - tipsiz.Height );

                if ( tiploc.X + tipsiz.Width > Parent.Area.Width - Parent.Margins.Right )
                {
                    tiploc.X = Parent.Area.Width - tipsiz.Width - Parent.Margins.Right;
                }

                Area = new Rectangle( tiploc, tipsiz );
                Visible = true;
            }
            else
            {
                Despawn();
            }
        }

        public void Despawn()
        {
            Visible = false;
        }

        protected internal override void OnLoad( Context context )
        {
        }

        protected internal override void OnUpdate( Context context )
        {
        }

        protected internal override void OnDraw( Context context )
        {
            base.OnDraw( context );

            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;

            SharedBrush.Color = AccentColor;
            TextStyle.Alignment = TextStyleAlignment.CenterCenter;
            context.DrawText( tip, TextStyle, clip, SharedBrush );

            context.AntialiasMode = AntialiasMode.Aliased;
            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2f );
        }
    }
}
