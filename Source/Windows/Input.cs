﻿using System;

namespace Kodo.Graphics.Style
{
#pragma warning disable CS1591
    /// <summary> Specifies the different keyboard keys. </summary>
    public enum Key
    {
        Modifiers = -65536,
        None = 0,
        LButton = 1, RButton = 2,
        Cancel = 3,
        MButton = 4, XButton1 = 5, XButton2 = 6,
        Back = 8, Tab = 9,
        LineFeed = 10, Clear = 12,
        Enter = 13, NumpadEnter,
        Pause = 19, CapsLock = 20,
        Escape = 27, Space = 32,
        PageUp = 33, Next = 34,
        PageDown = 34, End = 35, Home = 36,
        ArrowLeft = 37, ArrowUp = 38,
        ArrowRight = 39, ArrowDown = 40,
        Select = 41, Print = 42,
        Execute = 43, PrintScreen = 44,
        Insert = 45, Delete = 46, Help = 47,
        D0 = 48, D1 = 49, D2 = 50, D3 = 51,
        D4 = 52, D5 = 53, D6 = 54, D7 = 55,
        D8 = 56, D9 = 57,
        A = 65, B = 66, C = 67, D = 68,
        E = 69, F = 70, G = 71, H = 72,
        I = 73, J = 74, K = 75, L = 76,
        M = 77, N = 78, O = 79, P = 80,
        Q = 81, R = 82, S = 83, T = 84,
        U = 85, V = 86, W = 87, X = 88,
        Y = 89, Z = 90,
        LWin = 91, RWin = 92,
        Apps = 93,
        Sleep = 95,
        NumPad0 = 96, NumPad1 = 97,
        NumPad2 = 98, NumPad3 = 99,
        NumPad4 = 100, NumPad5 = 101,
        NumPad6 = 102, NumPad7 = 103,
        NumPad8 = 104, NumPad9 = 105,
        Multiply = 106, Add = 107,
        Separator = 108, Subtract = 109,
        Decimal = 110, Divide = 111,
        F1 = 112, F2 = 113, F3 = 114, F4 = 115,
        F5 = 116, F6 = 117, F7 = 118, F8 = 119,
        F9 = 120, F10 = 121, F11 = 122, F12 = 123,
        F13 = 124, F14 = 125, F15 = 126, F16 = 127,
        F17 = 128, F18 = 129, F19 = 130, F20 = 131,
        F21 = 132, F22 = 133, F23 = 134, F24 = 135,
        NumLock = 144, Scroll = 145,
        Shift = 0x10, Ctrl = 0x11,
        Alt = 0x12,
        ShiftLeft = 160, ShiftRight = 161,
        CtrlLeft = 162, CtrlRight = 163,
        AltLeft = 164, AltRight = 165,
        BrowserBack = 166, BrowserForward = 167,
        BrowserRefresh = 168, BrowserStop = 169,
        BrowserSearch = 170, BrowserFavorites = 171, BrowserHome = 172,
        VolumeMute = 173, VolumeDown = 174, VolumeUp = 175,
        MediaNextTrack = 176, MediaPreviousTrack = 177,
        MediaStop = 178, MediaPlayPause = 179,
        LaunchMail = 180,
        SelectMedia = 181,
        LaunchApplication1 = 182,
        LaunchApplication2 = 183,
        Oem1 = 186, OemSemicolon = 186,
        Oemplus = 187, Oemcomma = 188,
        OemMinus = 189, OemPeriod = 190,
        Oem2 = 191, OemQuestion = 191,
        Oemtilde = 192, Oem3 = 192,
        Oem4 = 219, OemOpenBrackets = 219,
        OemPipe = 220, Oem5 = 220,
        Oem6 = 221, OemCloseBrackets = 221,
        Oem7 = 222, OemQuotes = 222,
        Oem8 = 223, Oem102 = 226, OemBackslash = 226,
        ProcessKey = 229,
        Packet = 231,
        Attn = 246,
        Crsel = 247,
        Exsel = 248,
        EraseEof = 249,
        Play = 250,
        Zoom = 251,
        NoName = 252,
        Pa1 = 253,
        OemClear = 254
    }
#pragma warning restore CS1591

    /// <summary> Specifies the different mouse buttons. </summary>
    [Flags]
    public enum MouseButton
    {
        /// <summary> No button active. </summary>
        None = 0,
        /// <summary> The left button. </summary>
        Left = 0x0001,
        /// <summary> The right button. </summary>
        Right = 0x0002,
        /// <summary> The keyboard-key shift. </summary>
        Shift = 0x0004,
        /// <summary> The keyboard-key ctrl. </summary>
        Ctrl = 0x0008,
        /// <summary> The middle button. </summary>
        Middle = 0x0010,
        /// <summary> The first X button. </summary>
        X1 = 0x0020,
        /// <summary> The second X button. </summary>
        X2 = 0x0040
    }

    /// <summary> Specifies the different key modifiers. </summary>
    [Flags]
    public enum KeyModifier
    {
        /// <summary> If this bit is set, no modifiers are pressed. </summary>
        None = 0,
        /// <summary> If this bit is set, one or more Shift keys is held down. </summary>
        Shift = 0x0001,
        /// <summary> If this bit is set, one or more Control keys is held down. </summary>
        Ctrl = 0x0002,
        /// <summary> If this bit is set, one or more Alt keys is held down. </summary>
        Alt = 0x0004,
        /// <summary> If this bit is set, one or more Windows keys is held down. </summary>
        Win = 0x0008
    }

    /// <summary> Utility class for the keyboard. </summary>
    public static class Keyboard
    {
        static KeyModifier GetModifiers()
        {
            var pressedFlag = 1 << 31;
            var mods = (int)KeyModifier.None;

            if ( (Windows.GetKeyState( Windows.VK_SHIFT ) & pressedFlag) != 0 )
                mods |= (int)KeyModifier.Shift;
            if ( (Windows.GetKeyState( Windows.VK_CONTROL ) & pressedFlag) != 0 )
                mods |= (int)KeyModifier.Ctrl;
            if ( (Windows.GetKeyState( Windows.VK_MENU ) & pressedFlag) != 0 )
                mods |= (int)KeyModifier.Alt;
            if ( (Windows.GetKeyState( Windows.VK_LWIN ) != 0) | ((Windows.GetKeyState( Windows.VK_RWIN ) & pressedFlag) != 0) )
                mods |= (int)KeyModifier.Win;

            return (KeyModifier)mods;
        }

        /// <summary> Indicates whether or not the specified modifier key is pressed. </summary>
        public static bool IsDown( KeyModifier modifier )
        {
            return (GetModifiers() & modifier) == modifier;
        }

        /// <summary> Indicates whether or not the specified key is pressed. </summary>
        public static bool IsDown( Key key )
        {
            return (Windows.GetKeyState( (uint)key ) & 0x00008000) == 0x00008000;
        }

        /// <summary> Indicates whether or not the specified key is not pressed. </summary>
        public static bool IsUp( Key key )
        {
            return (Windows.GetKeyState( (uint)key ) & 0x00008000) != 0x00008000;
        }

        /// <summary> Indicates whether or not the specified key is toggled. </summary>
        public static bool IsToggled( Key key )
        {
            return (Windows.GetKeyState( (uint)key ) & 0x00000001) == 0x00000001;
        }

        public static Key MessageToKey( IntPtr wParam, IntPtr lParam )
        {
            var key = (uint)wParam;
            var lint = (UInt64)lParam;

            switch ( key )
            {
                // The SHIFT keys require special handling.
                case Windows.VK_SHIFT:
                    {
                        // Compare scan code for this key with that of VK_RSHIFT in order to determine which shift key was pressed (left or right).
                        var scancode = Windows.MapVirtualKey( Windows.VK_RSHIFT, 0 );

                        if ( (uint)((lint & 0x01ff0000) >> 16) == scancode )
                            return Key.ShiftRight;
                        return Key.ShiftLeft;
                    }
                // The CTRL keys require special handling.
                case Windows.VK_CONTROL:
                    {
                        if ( (lint & 0x01000000) != 0 )
                            return Key.CtrlRight;

                        // Here is a trick: "Alt Gr" sends LCTRL, then RALT. 
                        // We only want the RALT message, so we try to see if the next message is a RALT message. In that case, this is a false LCTRL!.
                        Windows.NativeMessage next;
                        uint time = Windows.GetMessageTime();

                        if ( Windows.PeekMessage( out next, IntPtr.Zero, 0, 0, Windows.PeekMessageFlags.PM_NOREMOVE ) )
                        {
                            if ( next.msg == Windows.WM_KEYDOWN ||
                                 next.msg == Windows.WM_SYSKEYDOWN ||
                                 next.msg == Windows.WM_KEYUP ||
                                 next.msg == Windows.WM_SYSKEYUP )
                            {
                                if ( (uint)next.wParam == Windows.VK_MENU && (((uint)next.lParam & 0x01000000) != 0) && next.time == time )
                                {
                                    // Next message is a RALT down message, which means that this is not a proper LCTRL message.
                                    return Key.None;
                                }
                            }
                        }

                        return Key.CtrlLeft;
                    }
                // The ALT keys require special handling.
                case Windows.VK_MENU:
                {
                    // Is this an extended key (i.e. right key)?
                        if ( (lint & 0x01000000) != 0 )
                            return Key.AltRight;
                    return Key.AltLeft;
                }
                // The ENTER keys require special handling.
                case Windows.VK_RETURN:
                {
                    // Is this an extended key (i.e. right key)?
                        if ( (lint & 0x01000000) != 0 )
                            return Key.NumpadEnter;
                    return Key.Enter;
                }
            }

            // Convert to the proper format.
            return (Key)key;
        }
    }

    /// <summary> Represents mouse input information. </summary>
    public struct Mouse
    {
        /// <summary> The button(s). </summary>
        public MouseButton Button;

        /// <summary> The mouse position. </summary>
        public Point Position;

        /// <summary> The wheel delta. </summary>
        public int WheelDelta;
        /// <summary> The vertical wheel movement. </summary>
        public double WheelVertical;
        /// <summary> The horizontal wheel movement. </summary>
        public double WheelHorizontal;

        /// <summary> Get the current mouse position. </summary>
        public static Point GetPosition()
        {
            Windows.POINT p;
            Windows.GetCursorPos( out p );
            return new Point( p.x, p.y );
        }

        /// <summary> Indicates whether or not the button/buttons is pressed. </summary>
        public bool IsPressed( MouseButton button )
        {
            return (Button & button) == button;
        }

        /// <summary> Create a new instance of Mouse. </summary>
        public Mouse( uint msg, IntPtr wParam, IntPtr lParam )
        {
            // Get the pressed buttons.
            Button = (MouseButton)Windows.LOWORD( wParam );

            // Get the coordinates.
            Position = new Point( Windows.GET_X_LPARAM( lParam ), Windows.GET_Y_LPARAM( lParam ) );

            if ( msg == Windows.WM_MOUSEWHEEL || msg == Windows.WM_MOUSEHWHEEL )
            {
                WheelDelta = Windows.GET_WHEEL_DELTA_WPARAM( wParam );

                var wheelMovement = (double)WheelDelta / Windows.WHEEL_DELTA;

                if ( msg == Windows.WM_MOUSEWHEEL )
                {
                    WheelHorizontal = 0.0;
                    WheelVertical = wheelMovement;
                }
                else if ( msg == Windows.WM_MOUSEHWHEEL )
                {
                    WheelVertical = 0.0;
                    WheelHorizontal = wheelMovement;
                }
                else
                {
                    WheelDelta = 0;
                    WheelHorizontal = 0;
                    WheelVertical = 0;
                }
            }
            else
            {
                WheelDelta = 0;
                WheelHorizontal = 0;
                WheelVertical = 0;
            }
        }
    }
}
