﻿using System;

namespace Kodo.Graphics.Style
{
    /// <summary> 
    /// A stopwatch timer. 
    /// </summary>
    public sealed class Counter
    {
        const long TicksPerMillisecond = 10000;
        const long TicksPerSecond = TicksPerMillisecond * 1000;

        bool isHighResolution;
        bool isRunning;
        long timeStart;
        long timeElapsed;
        double resolution;

        /// <summary>
        /// Amount of time elapsed since the start.
        /// </summary>
        public long Elapsed
        {
            get
            {
                var rawTicks = timeElapsed;

                if ( isRunning )
                {
                    // If the counter is running, add elapsed time since the counter is started last time. 
                    var timeCurrent = GetTime();
                    var elapsedUntilNow = timeCurrent - timeStart;

                    rawTicks += elapsedUntilNow;
                }

                // Convert high resolution perf counter to time ticks
                if ( isHighResolution )
                {
                    return (long)(rawTicks * resolution);
                }
                return rawTicks;
            }
        }

        /// <summary>
        /// Create a <see cref="Counter"/>.
        /// </summary>
        public Counter()
        {
            isRunning = false;
            isHighResolution = false;
            timeStart = 0L;
            timeElapsed = 0L;
            resolution = 0L;

            var frequency = 0L;
            var success = Windows.QueryPerformanceFrequency( out frequency );

            if ( success )
            {
                isHighResolution = true;
                resolution = TicksPerSecond;
                resolution /= frequency;
            }
            else
            {
                isHighResolution = false;
                resolution = 1;
            }
        }

        /// <summary>
        /// Start the counter.
        /// </summary>
        public void Start()
        {
            if ( !isRunning )
            {
                timeStart = GetTime();
                isRunning = true;
            }
        }

        /// <summary>
        /// Restart the counter.
        /// </summary>
        public void Restart()
        {
            timeElapsed = 0;
            timeStart = GetTime();
            isRunning = true;
        }

        /// <summary>
        /// Reset the counter.
        /// </summary>
        public void Reset()
        {
            isRunning = false;
            timeStart = 0;
            timeElapsed = 0;
        }

        /// <summary>
        /// Stop the counter.
        /// </summary>
        public void Stop()
        {
            if ( isRunning )
            {
                var timeEnd = GetTime();
                var timePeriod = timeEnd - timeStart;

                timeElapsed += timePeriod;
                isRunning = false;

                if ( timeElapsed < 0 )
                {
                    // When measuring small time periods the elapsed can return negative values.  
                    // This is due to bugs in the basic input/output system (BIOS) or the hardware abstraction layer (HAL) on machines with variable-speed CPUs (e.g. Intel SpeedStep).
                    timeElapsed = 0;
                }
            }
        }

        long GetTime()
        {
            if ( isHighResolution )
            {
                var counter = 0L;
                Windows.QueryPerformanceCounter( out counter );
                return counter;
            }
            return DateTime.UtcNow.Ticks;
        }
    }

    /// <summary> 
    /// A timer.
    /// </summary>
    public sealed class Timer : IDisposable
    {
        static uint TimerID = 1;

        Listener listener;
        DefaultEventHandler tickHandler;

        IntPtr id;
        uint interval;
        bool stoppingTimer;

        readonly object syncObject = new object();

        /// <summary> 
        /// Indicates whether or not the timer is running. 
        /// </summary>
        public bool IsRunning
        {
            get { return id != IntPtr.Zero; }
            set
            {
                lock ( syncObject )
                {
                    // Only change the state if it actually should change.
                    if ( value != IsRunning )
                    {
                        if ( value )
                        {
                            if ( id == IntPtr.Zero && !stoppingTimer )
                            {
                                // Start the timer.
                                id = Windows.SetTimer( listener.Handle, new IntPtr( TimerID++ ), interval, IntPtr.Zero );
                            }
                        }
                        else
                        {
                            if ( stoppingTimer )
                            {
                                return;
                            }

                            if ( id != IntPtr.Zero )
                            {
                                try
                                {
                                    // Stop the timer.
                                    stoppingTimer = true;
                                    Windows.KillTimer( listener.Handle, id );
                                }
                                finally
                                {
                                    id = IntPtr.Zero;
                                    stoppingTimer = false;
                                }
                            }
                        }
                    }

                }
            }
        }

        /// <summary> 
        /// Get or set the timer interval.
        /// </summary>
        public uint Interval
        {
            get { return interval; }
            set
            {
                lock ( syncObject )
                {
                    if ( value != interval )
                    {
                        interval = value;

                        if ( IsRunning )
                        {
                            // Restart the timer.
                            IsRunning = false;
                            IsRunning = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create a <see cref="Timer"/>.
        /// </summary>
        /// <param name="tickHandler">Delegate to be called on the timer tick.</param>
        public Timer( DefaultEventHandler tickHandler )
        {
            this.tickHandler = tickHandler;
            listener = new Listener( OnMessage );
        }

        void IDisposable.Dispose()
        {
            listener.Dispose();
        }

        /// <summary> 
        /// Start the timer. 
        /// </summary>
        public void Start()
        {
            IsRunning = true;
        }

        /// <summary> 
        /// Stop the timer. 
        /// </summary>
        public void Stop()
        {
            IsRunning = false;
        }

        bool OnMessage( ref Message message )
        {
            if ( message.Msg == Windows.WM_TIMER )
            {
                tickHandler();
                return true;
            }
            return false;
        }
    }
}
