﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace kodo_graphics_2_style_sandbox
{
    class Wnd : Window
    {
        ContextMenu menu;
        Button button;

        public Wnd( WindowManager manager )
            : base( manager )
        {
            button = new Button( this );
            button.Text = "testing";
            button.Area = Rectangle.FromXYWH( 100, 100, 100, 100 );
            menu = new ContextMenu( this );
            menu.OnContextMenu += OnContext;
            menu.Add( "View" );
            menu.AddSeparator();
            var subMenu = menu.AddSub( "Add to playlist..." );
            subMenu.Add( "New" );
            subMenu.AddSeparator();
            subMenu.Add( "Itemone" );
            subMenu.Add( "Itemtwo" );
            subMenu.Add( "Itemthree" );
            menu.AddSeparator();
            menu.Add( "Exit" );

            button.Menu = menu;
        }

        void OnContext( ContextMenuItem item )
        {

        }

        protected override bool OnMouseHover( Mouse mouse )
        {
            ShowTooltip( "This is a test." );
            return false;
        }
    }

    class Sandbox
    {
        static void Main( string[] args )
        {
            using ( var manager = new WindowManager( new ExceptionDelegate( Handler ) ) )
            {
                var window = new Wnd( manager );
                manager.Run( window );
            }
        }

        static void Handler( Exception e )
        {

        }
    }
}
