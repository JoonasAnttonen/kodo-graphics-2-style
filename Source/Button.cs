﻿namespace Kodo.Graphics.Style
{
    /// <summary>
    /// A button control.
    /// </summary>
    public class Button : Control
    {
        BitmapAsset icon;
        Bitmap iconBitmap;

        bool toggled;
        bool toggleOnClick;

        bool mouseIsHovering;
        bool mouseIsDown;

        string text;

        TextStyleAlignment textAlignment = TextStyleAlignment.CenterCenter;

        /// <summary> 
        /// Occurs when the button is clicked.
        /// </summary>
        public event DefaultEventHandler OnClick;

        /// <summary>
        /// Get or set the <see cref="TextStyleAlignment"/> of the button.
        /// </summary>
        public TextStyleAlignment TextAlignment
        {
            get { return textAlignment; }
            set
            {
                textAlignment = value;
                Update();
            }
        }

        /// <summary>
        /// Get or set the text displayed on the button.
        /// </summary>
        public string Text
        {
            get { return text; }
            set
            {
                if ( text != value )
                {
                    text = value;
                    Update();
                }
            }
        }

        /// <summary>
        /// Get or set the icon displayed on the butto.
        /// </summary>
        public BitmapAsset Icon
        {
            get { return icon; }
            set
            {
                if ( icon != value )
                {
                    icon = value;
                    Load();
                }
            }
        }

        /// <summary>
        /// Get or set whether or not the button gets toggled when clicked.
        /// </summary>
        public bool ToggleOnClick
        {
            get { return toggleOnClick; }
            set { toggleOnClick = value; }
        }

        /// <summary>
        /// Get or set the toggled state of the button.
        /// </summary>
        public bool Toggled
        {
            get { return toggled; }
            set
            {
                if ( toggled != value )
                {
                    toggled = value;
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Create a new <see cref="Button"/>.
        /// </summary>
        /// <param name="window">The parent <see cref="Window"/>.</param>
        public Button( Window window )
            : base( window )
        {
        }

        protected internal override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );
            context.FillRectangle( clip, SharedStyle.Foreground );

            var iconArea = new Rectangle();

            if ( iconBitmap != null )
            {
                iconArea = new Rectangle( icon.Size ).CenterTo( clip );

                if ( !string.IsNullOrEmpty( text ) )
                {
                    iconArea = iconArea.Move( clip.Left + 2 - iconArea.Left, 0 );
                }

                context.AntialiasMode = AntialiasMode.Aliased;
                SharedBrush.Color = Color.GhostWhite;

                SharedBrush.Color = toggled ? AccentColor : Color.GhostWhite;
                context.FillOpacityMask( iconBitmap, SharedBrush, iconArea );

                if ( mouseIsDown )
                {
                    SharedBrush.Color = Color.FromAColor( 0.5f, Color.GhostWhite );
                    context.FillOpacityMask( iconBitmap, SharedBrush, iconArea );
                }
                else if ( mouseIsHovering )
                {
                    SharedBrush.Color = Color.FromAColor( 0.5f, Color.SlateGray );
                    context.FillOpacityMask( iconBitmap, SharedBrush, iconArea );
                }
            }

            if ( !string.IsNullOrEmpty( text ) )
            {
                var textArea = new Rectangle( iconArea.Right + 2, clip.Top, clip.Right, clip.Bottom );

                SharedBrush.Color = toggled ? AccentColor : Color.GhostWhite;
                TextStyle.Alignment = TextAlignment;
                context.DrawText( text, TextStyle, textArea, SharedBrush );
            }

            if ( mouseIsDown )
            {
                context.FillRectangle( clip, SharedStyle.ForegroundPress );
            }
            else if ( mouseIsHovering )
            {
                context.FillRectangle( clip, SharedStyle.ForegroundHover );
            }

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2 );
        }

        protected internal override void OnLoad( Context context )
        {
            if ( icon != null )
            {
                iconBitmap = new Bitmap( context, icon.Source );
            }
        }

        protected internal override void OnMouseEnter( Mouse mouse )
        {
            base.OnMouseEnter( mouse );

            mouseIsHovering = true;
            Refresh();
        }

        protected internal override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );

            if ( mouseIsDown )
            {
                mouseIsDown = false;
                Refresh();

                if ( toggleOnClick )
                {
                    Toggled = !Toggled;
                }

                OnClick?.Invoke();
            }
        }

        protected internal override void OnMouseDoubleClick( Mouse mouse )
        {
            base.OnMouseDoubleClick( mouse );

            if ( mouse.IsPressed( MouseButton.Left ) )
            {
                mouseIsDown = true;
                Refresh();
            }
        }

        protected internal override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            if ( mouse.IsPressed( MouseButton.Left ) )
            {
                mouseIsDown = true;
                Refresh();
            }
        }

        protected internal override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );

            mouseIsDown = false;
            mouseIsHovering = false;

            Refresh();
        }
    }
}
