﻿using System;

namespace Kodo.Graphics.Style
{
    [Flags]
    public enum TrackbarStyle
    {
        None = 0,
        TrackRegular = 1,
        TrackFull = 2,
        ThumbAutoHide = 4,
        ThumbDisabled
    }

    public enum TrackbarOrientation
    {
        Horizontal,
        Vertical
    }

    /// <summary>
    /// A styled trackbar control.
    /// </summary>
    public class Trackbar : Control
    {
        // Fields
        //______________________________________________________________________________________

        TrackbarStyle style;
        TrackbarOrientation orientation;

        Rectangle thumbArea;
        float thumbPositionShift = 10f;
        float thumbPositionMin;
        float thumbPositionMax;
        bool thumbGripped;
        bool thumbVisible = true;
        bool mouseIsHovering;
        int thumbSizeStorage;
        int thumbSize = 20;

        int value = 50;
        int valueLast = 50;
        int valueMin;
        int valueMax = 100;
        float valueRange;

        // Events
        //______________________________________________________________________________________

        public delegate void OnScroll( int value );

        /// <summary>
        /// Occurs when the scrollbar value has changed.
        /// </summary>
        public event OnScroll Scroll;

        // Attributes
        //______________________________________________________________________________________

        /// <summary>
        /// Get or set the scrollbar orientation.
        /// </summary>
        public TrackbarStyle Style
        {
            get { return style; }
            set
            {
                if ( style != value )
                {
                    style = value;

                    if ( style.HasFlag( TrackbarStyle.ThumbAutoHide ) )
                    {
                        ThumbVisible = false;
                    }
                    else
                    {
                        ThumbVisible = true;
                    }

                    Update();
                }
            }
        }

        /// <summary>
        /// Get or set the scrollbar orientation.
        /// </summary>
        public TrackbarOrientation Orientation
        {
            get { return orientation; }
            set
            {
                if ( orientation != value )
                {
                    orientation = value;

                    Update();
                }
            }
        }

        public bool ThumbVisible
        {
            get { return thumbVisible; }
            set
            {
                if ( thumbVisible != value )
                {
                    if ( value )
                    {
                        thumbVisible = true;
                        ThumbSize = thumbSizeStorage;
                    }
                    else
                    {
                        thumbSizeStorage = ThumbSize;
                        ThumbSize = 0;
                        thumbVisible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Get or set the thumb width or height, depending on the orientation setting.
        /// </summary>
        public int ThumbSize
        {
            get { return thumbSize; }
            set
            {
                if ( thumbVisible )
                {
                    if ( thumbSize != value )
                    {
                        thumbSize = value;

                        Update();
                    }
                }
                else
                {
                    thumbSizeStorage = value;
                }

            }
        }

        /// <summary>
        /// Indicates whether or not the thumb is gripped.
        /// </summary>
        public bool ThumbGripped
        {
            get { return thumbGripped; }
        }

        /// <summary>
        /// Get or set the lower bound of the value.
        /// </summary>
        public int Minimum
        {
            get { return valueMin; }
            set
            {
                if ( valueMin != value )
                {
                    Set( this.value, value, valueMax );
                }
            }
        }

        /// <summary>
        /// Get or set the upper bound of the value.
        /// </summary>
        public int Maximum
        {
            get { return valueMax; }
            set
            {
                if ( valueMax != value )
                {
                    Set( this.value, valueMin, value );
                }
            }
        }

        /// <summary>
        /// Get or set the current value.
        /// </summary>
        public int Value
        {
            get { return value; }
            set
            {
                if ( this.value != value )
                {
                    Set( value, valueMin, valueMax );
                }
            }
        }

        // Publics
        //______________________________________________________________________________________

        public Trackbar( Window window )
            : base( window )
        {
        }

        static int Clamp( int value, int min, int max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        static float Clamp( float value, float min, float max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        /// <summary>
        /// Set value, minimum and maximum all at once to avoid redundant rendering.
        /// </summary>
        public void Set( int value, int minimum, int maximum )
        {
            valueMin = minimum;
            valueMax = maximum;
            this.value = Clamp( value, minimum, maximum );

            Refresh();
        }

        // Private Methods
        //______________________________________________________________________________________

        void CalculateValues( Mouse mouse )
        {
            float mousePos = orientation == TrackbarOrientation.Horizontal ? mouse.Position.X : mouse.Position.Y;
            float thumbCenter = 0f;

            // Restrict thumb position.
            thumbCenter = Clamp( mousePos - thumbPositionShift, thumbPositionMin, thumbPositionMax );

            if ( valueRange > 0 )
            {
                // Calculate and set the new value.
                var newValue = (int)Math.Round( thumbCenter / valueRange * (valueMax - valueMin) + valueMin );

                if ( valueLast != newValue )
                {
                    valueLast = newValue;
                    value = newValue;

                    Refresh();

                    // Fire the Scroll event.
                    if ( Scroll != null )
                    {
                        Scroll( value );
                    }
                }
            }
        }

        // Input
        //______________________________________________________________________________________

        protected internal override void OnMouseEnter( Mouse mouse )
        {
            base.OnMouseEnter( mouse );

            mouseIsHovering = true;

            if ( style.HasFlag( TrackbarStyle.ThumbAutoHide ) )
            {
                ThumbVisible = true;
            }
            else
            {
                Refresh();
            }
        }

        protected internal override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );

            mouseIsHovering = false;

            if ( style.HasFlag( TrackbarStyle.ThumbAutoHide ) )
            {
                ThumbVisible = false;
            }
            else
            {
                Refresh();
            }
        }

        protected internal override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );

            if ( thumbGripped )
            {
                // Not gripped anymore.
                thumbGripped = false;
                Refresh();

                // Fire the Scroll event.
                if ( Scroll != null )
                {
                    Scroll( value );
                }
            }
        }

        protected internal override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            // Only allow left button dragging and clicking.
            if ( mouse.IsPressed( MouseButton.Left ) )
            {
                // Is the mouse inside the thumb area?
                if ( thumbArea.Contains( mouse.Position ) )
                {
                    if ( orientation == TrackbarOrientation.Horizontal )
                    {
                        thumbPositionShift = mouse.Position.X - thumbArea.Left;
                    }
                    else
                    {
                        thumbPositionShift = mouse.Position.Y - thumbArea.Top;
                    }
                }
                else
                {
                    thumbPositionShift = thumbSize / 2f;

                    CalculateValues( mouse );
                }

                // Currently, if the left mouse button is pressed, thumb will always get gripped.
                thumbGripped = true;
                Refresh();
            }
        }

        protected internal override void OnMouseMove( Mouse mouse )
        {
            base.OnMouseMove( mouse );

            // Only proceed if the thumb is gripped.
            if ( thumbGripped )
            {
                CalculateValues( mouse );
            }
        }

        // Graphics
        //______________________________________________________________________________________

        protected internal override void OnLoad( Context context )
        {
        }

        protected internal override void OnUpdate( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            if ( orientation == TrackbarOrientation.Horizontal )
            {
                valueRange = clip.Width - thumbSize;
                thumbPositionMin = clip.Left;
                thumbPositionMax = clip.Right - thumbSize;
            }
            else
            {
                valueRange = clip.Height - thumbSize;
                thumbPositionMin = clip.Left;
                thumbPositionMax = clip.Bottom - thumbSize;
            }
        }

        protected internal override void OnDraw( Context context )
        {
            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1f;

            var pos = thumbPositionMin;
            var realRange = valueMax - valueMin;

            var trackTwo = SharedBrush.Color = Color.FromAColor( 0.05f, Color.GhostWhite );
            var clip = new Rectangle( Area.Dimensions );

            if ( realRange > 0f )
            {
                // Calculate relative thumb position.
                pos = (float)(value - valueMin) / realRange;
                pos = Clamp( (float)Math.Round( pos * valueRange ), thumbPositionMin, thumbPositionMax );
            }

            if ( style.HasFlag( TrackbarStyle.TrackFull ) )
            {
                SharedBrush.Color = trackTwo;
            }
            else
            {
                SharedBrush.Color = Color.LightSkyBlue;
            }

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            if ( orientation == TrackbarOrientation.Horizontal )
            {
                context.FillRectangle( clip, SharedStyle.Foreground );

                var barArea = Rectangle.FromXYWH( 0f, 0f, clip.Width, 6f ).CenterTo( clip );

                // Calculate thumb location and size.
                thumbArea = Rectangle.FromXYWH( pos, barArea.Top, thumbSize, barArea.Height );

                // Draw the track.
                context.FillRectangle( Rectangle.FromLTRB( barArea.Left, barArea.Top, thumbArea.Left, barArea.Bottom ), SharedBrush );
                SharedBrush.Color = trackTwo;
                context.FillRectangle( Rectangle.FromLTRB( thumbArea.Right, barArea.Top, barArea.Right, barArea.Bottom ), SharedBrush );
            }
            else
            {
                // Calculate thumb location and size.
                thumbArea = Rectangle.FromXYWH( clip.Left, pos, clip.Width, thumbSize );

                // Draw the track.
                context.FillRectangle( Rectangle.FromLTRB( clip.Left, clip.Top, clip.Right, thumbArea.Top ), SharedBrush );
                SharedBrush.Color = trackTwo;
                context.FillRectangle( Rectangle.FromLTRB( clip.Left, thumbArea.Bottom, clip.Right, clip.Bottom ), SharedBrush );
            }

            if ( thumbGripped )
            {
                SharedBrush.Color = Color.LightSkyBlue;
                context.FillRectangle( thumbArea, SharedBrush );
                SharedBrush.Color = Color.Black;
                context.DrawRectangle( thumbArea, SharedBrush, 1f );
            }
            else
            {
                SharedBrush.Color = Color.GhostWhite;
                context.FillRectangle( thumbArea, SharedBrush );
                SharedBrush.Color = Color.Black;
                context.DrawRectangle( thumbArea, SharedBrush, 1f );
            }

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2f );
        }
    }
}