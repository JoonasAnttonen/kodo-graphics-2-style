﻿using System;
using System.Drawing.Imaging;
using System.IO;
using Kodo.Graphics.Imaging;

namespace Kodo.Graphics.Style
{
    public class BitmapAsset : IDisposable
    {
        readonly WicScaler scaler;
        readonly MemoryStream rawMemory;
        readonly WicStream rawStream;

        public WicBitmapSource Source => scaler;

        public Size Size
        {
            get;
            private set;
        }

        public BitmapAsset( string filename, Size size )
            : this( filename, (int)size.Width, (int)size.Height )
        {
        }
        public BitmapAsset( string filename, int imageWidth, int imageHeight )
        {
            rawStream = new WicStream( filename );
            Size = new Size( imageWidth, imageHeight );
            scaler = GetSource( rawStream, (int)Size.Width, (int)Size.Height );
        }

        public BitmapAsset( byte[] data, Size size )
            : this( data, (int)size.Width, (int)size.Height )
        {
        }
        public BitmapAsset( byte[] data, int imageWidth, int imageHeight )
        {
            rawMemory = new MemoryStream( data );
            rawStream = new WicStream( rawMemory );
            Size = new Size( imageWidth, imageHeight );
            scaler = GetSource( rawStream, imageWidth, imageHeight );
        }

        /// <summary>
        /// Create a new <see cref="BitmapAsset"/> from an <see cref="System.Drawing.Bitmap"/>.
        /// </summary>
        /// <param name="bitmap">The <see cref="System.Drawing.Bitmap"/>.</param>
        public BitmapAsset( System.Drawing.Bitmap bitmap )
            : this( bitmap, new Size( bitmap.Width, bitmap.Height ) )
        {
        }

        /// <summary>
        /// Create a new <see cref="BitmapAsset"/> from an <see cref="System.Drawing.Bitmap"/>.
        /// </summary>
        /// <param name="bitmap">The <see cref="System.Drawing.Bitmap"/>.</param>
        /// <param name="size">The <see cref="Size"/>.</param>
        public BitmapAsset( System.Drawing.Bitmap bitmap, Size size )
        {
            rawMemory = new MemoryStream();
            bitmap.Save( rawMemory, ImageFormat.Png );
            rawStream = new WicStream( rawMemory );
            Size = size;
            scaler = GetSource( rawStream, (int)size.Width, (int)size.Height );
        }

        WicScaler GetSource( WicStream stream, int width, int height )
        {
            using ( var decoder = new WicDecoder( stream, WicDecodeOptions.MetadataCacheOnLoad ) )
            using ( var frame = decoder.GetFrame( 0 ) )
            using ( var converter = new WicFormatConverter() )
            {
                converter.Convert( frame, WicPixelFormat.PBGRA32bpp );

                var scale = new WicScaler();
                scale.Scale( converter, width, height, WicInterpolationMode.Fant );
                return scale;
            }
        }

        public void Dispose()
        {
            rawMemory.Dispose();
            rawStream.Dispose();
            scaler.Dispose();
        }
    }
}
