﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Represents a method that handles a native message.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// <returns>Whether the message was handled or not.</returns>
    public delegate bool ListenerDelegate( ref Message msg );
    internal delegate IntPtr WndProcDelegate( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam );

    /// <summary>
    /// Message listener.
    /// </summary>
    public class Listener : IDisposable
    {
        static int ListenerCount;
        string wndClassName;

        int wndClass;
        IntPtr wndHandle;
        HandleRef wndProcRef;
        WndProcDelegate wndProcDelegate;
        ListenerDelegate listener;

        /// <summary>
        /// The native handle of the listener.
        /// </summary>
        public IntPtr Handle
        {
            get { return wndHandle; }
        }

        /// <summary>
        /// Create a <see cref="Listener"/>.
        /// </summary>
        /// <param name="msgHandler">Delegate to be called when a message is received.</param>
        public Listener( ListenerDelegate msgHandler )
        {
            Utility.ThrowIfNull( msgHandler );

            listener = msgHandler;
            wndClassName = $"kodo-listener-{Interlocked.Increment( ref ListenerCount )}";
            wndProcDelegate = WndProc;
            wndProcRef = new HandleRef( wndProcDelegate, Marshal.GetFunctionPointerForDelegate( wndProcDelegate ) );

            Windows.WNDCLASSEX classOptions = Windows.WNDCLASSEX.Make();
            classOptions.lpfnWndProc = wndProcRef.Handle;
            classOptions.hInstance = Windows.GetModuleHandle();
            classOptions.lpszClassName = wndClassName;

            wndClass = Windows.RegisterClassEx( ref classOptions );
            Utility.ThrowIfZero( wndClass );

            wndHandle = Windows.CreateWindowEx( 0, wndClassName, null, 0, 0, 0, 0, 0, Windows.HWND_MESSAGE, IntPtr.Zero, Windows.GetModuleHandle(), IntPtr.Zero );
            Utility.ThrowIfZero( wndHandle );
        }

        /// <summary>
        /// Destroy the <see cref="Listener"/>.
        /// </summary>
        public void Dispose()
        {
            if ( wndHandle != IntPtr.Zero )
                Windows.DestroyWindow( wndHandle );

            if ( wndClass != 0 )
                Windows.UnregisterClass( wndClassName, Windows.GetModuleHandle() );
        }

        IntPtr WndProc( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            var message = new Message( hWnd, msg, wParam, lParam );
            return listener( ref message ) ? IntPtr.Zero : Windows.DefWindowProc( hWnd, msg, wParam, lParam );
        }
    }
}
