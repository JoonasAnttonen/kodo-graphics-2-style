﻿using System;
using System.Collections.Generic;

namespace Kodo.Graphics.Style
{
    public class ContextMenuItem
    {
        public ContextMenu Parent { get; }
        public string Text { get; }

        public ContextMenu Menu { get; private set; }

        public ContextMenuItem( ContextMenu parent, string text, ContextMenu menu )
        {
            Parent = parent;
            Text = text;
            Menu = menu;
        }

        public ContextMenu AddMenu()
        {
            Menu = new ContextMenu( Parent );
            return Menu;
        }
    }

    public delegate void OnContextMenuItemDelegate( ContextMenuItem item );

    public class ContextMenu : Control
    {
        List<ContextMenuItem> items = new List<ContextMenuItem>();

        ContextMenu rootMenu;
        ContextMenu subMenu;
        int subSpawnedAt = -1;

        const int defaultWidth = 100;
        const int itemHeight = 30;
        const int separatorHeight = 10;
        float width;
        float height;
        int hoveredIndex = -1;

        public event OnContextMenuItemDelegate OnContextMenu;

        public ContextMenu( Window window )
            : base( window )
        {
            AlwaysOnTop = true;
            Visible = false;
        }

        public ContextMenu( ContextMenu parent )
            : base( parent.Parent )
        {
            rootMenu = parent;
            AlwaysOnTop = true;
            Visible = false;
        }

        public void AddSeparator()
        {
            Add( null, null );
        }

        public ContextMenu AddSub( string text )
        {
            return Add( text ).AddMenu();
        }

        public ContextMenuItem Add( string text, ContextMenu subMenu = null )
        {
            var result = new ContextMenuItem( this, text, subMenu );
            items.Add( result );

            Update();

            return result;
        }

        /// <summary>
        /// Show the <see cref="ContextMenu"/>.
        /// </summary>
        /// <param name="location">Location of the <see cref="ContextMenu"/>.</param>
        public void Show( Point location )
        {
            if ( Visible )
                return;

            var areaOfParent = new Rectangle( Parent.Area.Dimensions );
            var area = new Rectangle( location, new Size( width, height ) );

            if ( area.Right > areaOfParent.Right )
            {
                area = Rectangle.FromXYWH( location.X - (IsRoot ? width : width + rootMenu.width), area.Top, width, height );
            }
            if ( area.Bottom > areaOfParent.Bottom )
            {
                area = Rectangle.FromXYWH( area.Left, location.Y - (IsRoot ? height : height + rootMenu.height), width, height );
            }

            Area = area;
            Update();
            Visible = true;

            if ( IsRoot )
            {
                Parent.FocusMouse( this );
                Parent.FocusKeyboard( this );
            }
        }

        /// <summary>
        /// Hide the <see cref="ContextMenu"/>.
        /// </summary>
        public void Hide()
        {
            if ( !Visible )
                return;

            hoveredIndex = -1;
            subSpawnedAt = -1;

            if ( subMenu != null )
            {
                subMenu.Hide();
                subMenu = null;
            }

            Visible = false;

            if ( IsRoot )
            {
                Parent.ResetFocus();
            }
            else
            {
                Parent.FocusMouse( rootMenu );
                Parent.FocusKeyboard( rootMenu );
            }
        }

        void OnSub( ContextMenuItem item )
        {
            Hide();
        }

        protected internal override void OnMouseMove( Mouse mouse )
        {
            var x = 0;
            var y = 2;

            hoveredIndex = -1;

            if ( mouse.Position.X >= x && mouse.Position.X <= width - x && mouse.Position.Y >= y && mouse.Position.Y <= height - y )
            {
                for ( var i = 0; i < items.Count; i++ )
                {
                    var iH = string.IsNullOrEmpty( items[ i ].Text ) ? separatorHeight : itemHeight;

                    if ( mouse.Position.Y >= y && mouse.Position.Y <= y + iH )
                    {
                        if ( string.IsNullOrEmpty( items[ i ].Text ) )
                            break;

                        hoveredIndex = i;
                        break;
                    }

                    y += iH;
                }
            }

            if ( subMenu != null && hoveredIndex != -1 && subSpawnedAt != hoveredIndex )
            {
                subMenu.Hide();
                subSpawnedAt = -1;
                subMenu = null;
            }

            if ( hoveredIndex != -1 && items[ hoveredIndex ].Menu != null )
            {
                var item = items[ hoveredIndex ];

                if ( item.Menu != null )
                {
                    if ( subMenu != null )
                        subMenu.OnContextMenu -= OnSub;

                    subMenu = item.Menu;

                    var pos = FromControlToWindow( mouse.Position );
                    pos.Y = FromControlToWindow( new Point( 0, y ) ).Y;
                    pos.X = Area.Right;

                    subSpawnedAt = hoveredIndex;
                    subMenu.OnContextMenu += OnSub;
                    subMenu.Show( pos );
                }
            }

            Refresh();
        }

        bool IsRoot
        {
            get { return rootMenu == null; }
        }

        protected internal override void OnKeyDown( Key key )
        {
            if ( key == Key.Escape )
            {
                Hide();
            }

            base.OnKeyDown( key );
        }

        protected internal override void OnMouseDown( Mouse mouse )
        {
            var mousePositionOnWindow = FromControlToWindow( mouse.Position );

            if ( !Area.Contains( mousePositionOnWindow ) )
            {
                Hide();
            }
            else if ( hoveredIndex != -1 )
            {
                var item = items[ hoveredIndex ];

                if ( item.Menu == null )
                {
                    Hide();
                    OnContextMenu?.Invoke( item );
                }
            }
        }

        protected internal override void OnMouseEnter( Mouse mouse )
        {
            base.OnMouseEnter( mouse );

            Parent.FocusMouse( this );
            Parent.FocusKeyboard( this );
        }

        protected internal override void OnMouseLeave( Mouse mouse )
        {
            hoveredIndex = -1;

            if ( mouse.Button == MouseButton.Left || mouse.Button == MouseButton.Right )
            {
                if ( IsRoot )
                    Hide();
                else
                    rootMenu.Hide();
            }
        }

        protected internal override void OnUpdate( Context context )
        {
            width = defaultWidth;
            height = 0;

            foreach ( var item in items )
            {
                if ( string.IsNullOrEmpty( item.Text ) )
                {
                    height += separatorHeight;
                    width = (int)Math.Max( width, defaultWidth + 20 );
                }
                else
                {
                    var metrics = TextStyle.GetTextMetrics( item.Text );
                    width = (int)Math.Max( width, metrics.WidthIncludingTrailingWhitespace + 20 );
                    height += itemHeight;
                }
            }
        }

        protected internal override void OnDraw( Context context )
        {
            base.OnDraw( context );

            var clip = new Rectangle( Area.Dimensions );

            SharedBrush.Opacity = 1;

            var x = 5;
            var y = 0;

            SharedBrush.Color = Color.GhostWhite;
            TextStyle.Alignment = TextStyleAlignment.LeftCenter;

            for ( var i = 0; i < items.Count; i++ )
            {
                var item = items[ i ];

                if ( string.IsNullOrEmpty( item.Text ) )
                {
                    context.DrawLine( new Point( x, y + separatorHeight / 2 ), new Point( x + (width - x * 2), y + separatorHeight / 2 ), SharedBrush, 1 );
                    y += separatorHeight;
                }
                else
                {
                    context.DrawText( item.Text, TextStyle, Rectangle.FromXYWH( x, y, width, itemHeight ), SharedBrush );

                    if ( hoveredIndex == i || subSpawnedAt == i )
                    {
                        SharedBrush.Opacity = 0.2f;
                        context.FillRectangle( Rectangle.FromXYWH( 0, y, width, itemHeight ), SharedBrush );
                        SharedBrush.Opacity = 1;
                    }

                    y += itemHeight;
                }
            }

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2 );
        }
    }
}
