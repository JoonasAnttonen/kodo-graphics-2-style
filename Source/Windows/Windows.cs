﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Repsesents an empty event handler.
    /// </summary>
    public delegate void DefaultEventHandler();

    /// <summary>
    /// Represents a native message.
    /// </summary>
    public struct Message
    {
        /// <summary>
        /// 
        /// </summary>
        public IntPtr HWnd;
        /// <summary>
        /// 
        /// </summary>
        public uint Msg;
        /// <summary>
        /// 
        /// </summary>
        public IntPtr WParam;
        /// <summary>
        /// 
        /// </summary>
        public IntPtr LParam;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        public Message( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
        {
            HWnd = hWnd;
            Msg = msg;
            WParam = wParam;
            LParam = lParam;
        }
    }

    [Flags]
    public enum DialogFlags
    {
        OFN_READONLY = 0x1,
        OFN_OVERWRITEPROMPT = 0x2,
        OFN_HIDEREADONLY = 0x4,
        OFN_NOCHANGEDIR = 0x8,
        OFN_SHOWHELP = 0x10,
        OFN_ENABLEHOOK = 0x20,
        OFN_ENABLETEMPLATE = 0x40,
        OFN_ENABLETEMPLATEHANDLE = 0x80,
        OFN_NOVALIDATE = 0x100,
        OFN_ALLOWMULTISELECT = 0x200,
        OFN_EXTENSIONDIFFERENT = 0x400,
        OFN_PATHMUSTEXIST = 0x800,
        OFN_FILEMUSTEXIST = 0x1000,
        OFN_CREATEPROMPT = 0x2000,
        OFN_SHAREAWARE = 0x4000,
        OFN_NOREADONLYRETURN = 0x8000,
        OFN_NOTESTFILECREATE = 0x10000,
        OFN_NONETWORKBUTTON = 0x20000,
        /// <summary>
        /// Force no long names for 4.x modules
        /// </summary>
        OFN_NOLONGNAMES = 0x40000,
        /// <summary>
        /// New look commdlg
        /// </summary>
        OFN_EXPLORER = 0x80000,
        OFN_NODEREFERENCELINKS = 0x100000,
        /// <summary>
        /// Force long names for 3.x modules
        /// </summary>
        OFN_LONGNAMES = 0x200000,
    }

    internal static class Windows
    {
        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Auto )]
        public class OpenFileName
        {
            public int      structSize = 0;
            public IntPtr   dlgOwner = IntPtr.Zero;
            public IntPtr   instance = IntPtr.Zero;
            public string   filter = null;
            public string   customFilter = null;
            public int      maxCustFilter = 0;
            public int      filterIndex = 0;
            public string   file = null;
            public int      maxFile = 0;
            public string   fileTitle = null;
            public int      maxFileTitle = 0;
            public string   initialDir = null;
            public string   title = null;
            public int      flags = 0;
            public short    fileOffset = 0;
            public short    fileExtension = 0;
            public string   defExt = null;
            public IntPtr   custData = IntPtr.Zero;
            public IntPtr   hook = IntPtr.Zero;
            public string   templateName = null;
            public IntPtr   reservedPtr = IntPtr.Zero;
            public int      reservedInt = 0;
            public int      flagsEx = 0;
        }

        public enum NotifyIconMessage
        {
            NIM_ADD = 0x00000000,
            NIM_MODIFY = 0x00000001,
            NIM_DELETE = 0x00000002,
            NIM_SETFOCUS = 0x00000003,
            NIM_SETVERSION = 0x00000004
        }

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Auto )]
        public class NOTIFYICONDATA
        {
            public int cbSize = Marshal.SizeOf(typeof(NOTIFYICONDATA));
            public IntPtr hWnd;
            public int uID;
            public int uFlags;
            public int uCallbackMessage;
            public IntPtr hIcon;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst=0x80)]
            public string szTip;
            public int dwState;
            public int dwStateMask;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst=0x100)]
            public string szInfo;
            public int uTimeoutOrVersion;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst=0x40)]
            public string szInfoTitle;
            public int dwInfoFlags;
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct NCCALCSIZE_PARAMS
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public RECT[] rgrc;
            public WINDOWPOS lppos;
        }

        public static readonly IntPtr HWND_MESSAGE = new IntPtr( -3 );

        //
        // These are almost straight from the windows headers, no touchy, you will fuck it up.
        //

        public const int WVR_ALIGNLEFT = 0x0020;
        public const int WVR_ALIGNTOP = 0x0010;

        public static ushort LOWORD( IntPtr l ) { return (ushort)((ulong)l & 0xffff); }
        public static ushort HIWORD( IntPtr l ) { return (ushort)(((ulong)l >> 16) & 0xffff); }
        public static int GET_X_LPARAM( IntPtr lp ) { return (short)LOWORD( lp ); }
        public static int GET_Y_LPARAM( IntPtr lp ) { return (short)HIWORD( lp ); }
        public const int WHEEL_DELTA = 120;
        public static short GET_WHEEL_DELTA_WPARAM( IntPtr wParam ) { return (short)HIWORD( wParam ); }

        public const uint MONITOR_DEFAULTTONULL = 0;
        public const uint MONITOR_DEFAULTTOPRIMARY = 1;
        public const uint MONITOR_DEFAULTTONEAREST = 2;

        public const uint HTERROR = unchecked( (uint)-2 );
        public const uint HTTRANSPARENT = unchecked( (uint)-1 );
        public const uint HTNOWHERE = 0;
        public const uint HTCLIENT = 1;
        public const uint HTCAPTION = 2;
        public const uint HTSYSMENU = 3;
        public const uint HTGROWBOX = 4;
        public const uint HTSIZE = HTGROWBOX;
        public const uint HTMENU = 5;
        public const uint HTHSCROLL = 6;
        public const uint HTVSCROLL = 7;
        public const uint HTMINBUTTON = 8;
        public const uint HTMAXBUTTON = 9;
        public const uint HTLEFT = 10;
        public const uint HTRIGHT = 11;
        public const uint HTTOP = 12;
        public const uint HTTOPLEFT = 13;
        public const uint HTTOPRIGHT = 14;
        public const uint HTBOTTOM = 15;
        public const uint HTBOTTOMLEFT = 16;
        public const uint HTBOTTOMRIGHT = 17;
        public const uint HTBORDER = 18;
        public const uint HTREDUCE = HTMINBUTTON;
        public const uint HTZOOM = HTMAXBUTTON;
        public const uint HTSIZEFIRST = HTLEFT;
        public const uint HTSIZELAST = HTBOTTOMRIGHT;
        public const uint HTOBJECT = 19;
        public const uint HTCLOSE = 20;
        public const uint HTHELP = 21;

        public const uint SW_HIDE = 0;
        public const uint SW_SHOWNORMAL = 1;
        public const uint SW_NORMAL = 1;
        public const uint SW_SHOWMINIMIZED = 2;
        public const uint SW_SHOWMAXIMIZED = 3;
        public const uint SW_MAXIMIZE = 3;
        public const uint SW_SHOWNOACTIVATE = 4;
        public const uint SW_SHOW = 5;
        public const uint SW_MINIMIZE = 6;
        public const uint SW_SHOWMINNOACTIVE = 7;
        public const uint SW_SHOWNA = 8;
        public const uint SW_RESTORE = 9;
        public const uint SW_SHOWDEFAULT = 10;
        public const uint SW_FORCEMINIMIZE = 11;

        public const uint SWP_NOSIZE = 0x0001;
        public const uint SWP_NOMOVE = 0x0002;
        public const uint SWP_NOZORDER = 0x0004;
        public const uint SWP_NOREDRAW = 0x0008;
        public const uint SWP_NOACTIVATE = 0x0010;
        public const uint SWP_FRAMECHANGED = 0x0020;
        public const uint SWP_SHOWWINDOW = 0x0040;
        public const uint SWP_HIDEWINDOW = 0x0080;
        public const uint SWP_NOCOPYBITS = 0x0100;
        public const uint SWP_NOOWNERZORDER = 0x0200;
        public const uint SWP_NOSENDCHANGING = 0x0400;
        public const uint SWP_DRAWFRAME = SWP_FRAMECHANGED;
        public const uint SWP_NOREPOSITION = SWP_NOOWNERZORDER;
        public const uint SWP_DEFERERASE = 0x2000;
        public const uint SWP_ASYNCWINDOWPOS = 0x4000;

        public const uint CS_VREDRAW = 0x0001;
        public const uint CS_HREDRAW = 0x0002;
        public const uint CS_DBLCLKS = 0x0008;
        public const uint CS_OWNDC = 0x0020;
        public const uint CS_CLASSDC = 0x0040;
        public const uint CS_PARENTDC = 0x0080;
        public const uint CS_NOCLOSE = 0x0200;
        public const uint CS_SAVEBITS = 0x0800;
        public const uint CS_BYTEALIGNCLIENT = 0x1000;
        public const uint CS_BYTEALIGNWINDOW = 0x2000;
        public const uint CS_GLOBALCLASS = 0x4000;

        public const uint ICON_SMALL = 0;
        public const uint ICON_BIG = 1;

        public const uint WM_SETICON = 0x0080;

        public const uint WM_KEYFIRST = 0x0100;
        public const uint WM_KEYDOWN = 0x0100;
        public const uint WM_KEYUP = 0x0101;
        public const uint WM_CHAR = 0x0102;
        public const uint WM_DEADCHAR = 0x0103;
        public const uint WM_SYSKEYDOWN = 0x0104;
        public const uint WM_SYSKEYUP = 0x0105;
        public const uint WM_SYSCHAR = 0x0106;

        public const uint WM_MOUSEFIRST = 0x0200;
        public const uint WM_MOUSEMOVE = 0x0200;
        public const uint WM_LBUTTONDOWN = 0x0201;
        public const uint WM_LBUTTONUP = 0x0202;
        public const uint WM_LBUTTONDBLCLK = 0x0203;
        public const uint WM_RBUTTONDOWN = 0x0204;
        public const uint WM_RBUTTONUP = 0x0205;
        public const uint WM_RBUTTONDBLCLK = 0x0206;
        public const uint WM_MBUTTONDOWN = 0x0207;
        public const uint WM_MBUTTONUP = 0x0208;
        public const uint WM_MBUTTONDBLCLK = 0x0209;
        public const uint WM_MOUSEWHEEL = 0x020A;
        public const uint WM_XBUTTONDOWN = 0x020B;
        public const uint WM_XBUTTONUP = 0x020C;
        public const uint WM_XBUTTONDBLCLK = 0x020D;
        public const uint WM_MOUSEHWHEEL = 0x020E;

        public const uint WM_NCCREATE = 0x0081;
        public const uint WM_NCDESTROY = 0x0082;
        public const uint WM_NCCALCSIZE = 0x0083;
        public const uint WM_NCHITTEST = 0x0084;

        public const uint VK_LSHIFT = 0xA0;
        public const uint VK_RSHIFT = 0xA1;
        public const uint VK_LCONTROL = 0xA2;
        public const uint VK_RCONTROL = 0xA3;
        public const uint VK_LMENU = 0xA4;
        public const uint VK_RMENU = 0xA5;

        public const uint VK_SNAPSHOT = 0x2C;

        public const uint VK_RETURN = 0x0D;
        public const uint VK_SHIFT = 0x10;
        public const uint VK_CONTROL = 0x11;
        public const uint VK_MENU = 0x12;
        public const uint VK_PAUSE = 0x13;
        public const uint VK_CAPITAL = 0x14;
        public const uint VK_LWIN = 0x5B;
        public const uint VK_RWIN = 0x5C;

        public const uint WM_GETMINMAXINFO = 0x0024;
        public const uint WM_MOUSEHOVER = 0x02A1;
        public const uint WM_MOUSELEAVE = 0x02A3;
        public const uint WM_WINDOWPOSCHANGING = 0x0046;
        public const uint WM_WINDOWPOSCHANGED = 0x0047;

        public const uint WM_DWMCOMPOSITIONCHANGED = 0x031E;
        public const uint WM_DWMNCRENDERINGCHANGED = 0x031F;
        public const uint WM_DWMCOLORIZATIONCOLORCHANGED = 0x0320;
        public const uint WM_DWMWINDOWMAXIMIZEDCHANGE = 0x0321;

        public const uint WM_DROPFILES = 0x0233;

        public const uint WM_DISPLAYCHANGE = 0x007E;
        public const uint WM_CREATE = 0x0001;
        public const uint WM_DESTROY = 0x0002;
        public const uint WM_MOVE = 0x0003;
        public const uint WM_SIZE = 0x0005;

        public const uint WM_SETFOCUS = 0x0007;
        public const uint WM_KILLFOCUS = 0x0008;
        public const uint WM_ENABLE = 0x000A;
        public const uint WM_SETREDRAW = 0x000B;
        public const uint WM_SETTEXT = 0x000C;
        public const uint WM_GETTEXT = 0x000D;
        public const uint WM_GETTEXTLENGTH = 0x000E;
        public const uint WM_PAINT = 0x000F;
        public const uint WM_CLOSE = 0x0010;
        public const uint WM_QUERYENDSESSION = 0x0011;
        public const uint WM_QUERYOPEN = 0x0013;
        public const uint WM_ENDSESSION = 0x0016;

        public const uint WM_TIMER = 0x0113;
        public const uint WM_QUIT = 0x0012;
        public const uint WM_USER = 0x0400;

        public const string Comdlg32 = "Comdlg32.dll";
        public const string User32 = "user32.dll";
        public const string Kernel32 = "kernel32.dll";
        public const string Shell32 = "shell32.dll";

        [StructLayout( LayoutKind.Sequential )]
        public struct MONITORINFO
        {
            public int cbSize;
            public RECT rcMonitor;
            public RECT rcWork;
            public uint dwFlags;

            public static MONITORINFO Make()
            {
                var result = new MONITORINFO();
                result.cbSize = Marshal.SizeOf( typeof( MONITORINFO ) );
                return result;
            }
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct WINDOWPOS
        {
            public IntPtr hwnd;
            public IntPtr hwndInsertAfter;
            public int x;
            public int y;
            public int cx;
            public int cy;
            public uint flags;
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public uint showCmd;
            public POINT ptMinPosition;
            public POINT ptMaxPosition;
            public RECT rcNormalPosition;

            public static WINDOWPLACEMENT Make()
            {
                var result = new WINDOWPLACEMENT();
                result.length = Marshal.SizeOf( typeof( WINDOWPLACEMENT ) );
                return result;
            }
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct POINT
        {
            public int x;
            public int y;

            public POINT( int x, int y )
            {
                this.x = x;
                this.y = y;
            }
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct NativeMessage
        {
            public IntPtr handle;
            public uint msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct WNDCLASSEX
        {
            [MarshalAs( UnmanagedType.U4 )]
            public int cbSize;
            [MarshalAs( UnmanagedType.U4 )]
            public uint style;
            public IntPtr lpfnWndProc;
            public int cbClsExtra;
            public int cbWndExtra;
            public IntPtr hInstance;
            public IntPtr hIcon;
            public IntPtr hCursor;
            public IntPtr hbrBackground;
            public string lpszMenuName;
            public string lpszClassName;
            public IntPtr hIconSm;

            public static WNDCLASSEX Make()
            {
                var result = new WNDCLASSEX();
                result.cbSize = Marshal.SizeOf( typeof( WNDCLASSEX ) );
                return result;
            }
        }

        [Flags]
        public enum WindowStyles : uint
        {
            WS_BORDER = 0x800000,
            WS_CAPTION = 0xc00000,
            WS_CHILD = 0x40000000,
            WS_CLIPCHILDREN = 0x2000000,
            WS_CLIPSIBLINGS = 0x4000000,
            WS_DISABLED = 0x8000000,
            WS_DLGFRAME = 0x400000,
            WS_GROUP = 0x20000,
            WS_HSCROLL = 0x100000,
            WS_MAXIMIZE = 0x1000000,
            WS_MAXIMIZEBOX = 0x10000,
            WS_MINIMIZE = 0x20000000,
            WS_MINIMIZEBOX = 0x20000,
            WS_OVERLAPPED = 0x0,
            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_SIZEFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            WS_POPUP = 0x80000000u,
            WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
            WS_SIZEFRAME = 0x40000,
            WS_SYSMENU = 0x80000,
            WS_TABSTOP = 0x10000,
            WS_VISIBLE = 0x10000000,
            WS_VSCROLL = 0x200000
        }

        [Flags]
        public enum WindowStylesEx : uint
        {
            WS_EX_ACCEPTFILES = 0x00000010,
            WS_EX_APPWINDOW = 0x00040000,
            WS_EX_CLIENTEDGE = 0x00000200,
            WS_EX_COMPOSITED = 0x02000000,
            WS_EX_CONTEXTHELP = 0x00000400,
            WS_EX_CONTROLPARENT = 0x00010000,
            WS_EX_DLGMODALFRAME = 0x00000001,
            WS_EX_LAYERED = 0x00080000,
            WS_EX_LAYOUTRTL = 0x00400000,
            WS_EX_LEFT = 0x00000000,
            WS_EX_LEFTSCROLLBAR = 0x00004000,
            WS_EX_LTRREADING = 0x00000000,
            WS_EX_MDICHILD = 0x00000040,
            WS_EX_NOACTIVATE = 0x08000000,
            WS_EX_NOINHERITLAYOUT = 0x00100000,
            WS_EX_NOPARENTNOTIFY = 0x00000004,
            WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,
            WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST,
            WS_EX_RIGHT = 0x00001000,
            WS_EX_RIGHTSCROLLBAR = 0x00000000,
            WS_EX_RTLREADING = 0x00002000,
            WS_EX_STATICEDGE = 0x00020000,
            WS_EX_TOOLWINDOW = 0x00000080,
            WS_EX_TOPMOST = 0x00000008,
            WS_EX_TRANSPARENT = 0x00000020,
            WS_EX_WINDOWEDGE = 0x00000100
        }

        public enum PeekMessageFlags : uint
        {
            PM_NOREMOVE = 0x0000,
            PM_REMOVE = 0x0001,
            PM_NOYIELD = 0x0002
        }

        [Flags]
        public enum TMEFlags : uint
        {
            TME_CANCEL = 0x80000000,
            TME_HOVER = 0x00000001,
            TME_LEAVE = 0x00000002,
            TME_NONCLIENT = 0x00000010,
            TME_QUERY = 0x40000000
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct TRACKMOUSEEVENT
        {
            public int cbSize;
            [MarshalAs( UnmanagedType.U4 )]
            public TMEFlags dwFlags;
            public IntPtr hWnd;
            public uint dwHoverTime;

            public TRACKMOUSEEVENT( TMEFlags dwFlags, IntPtr hWnd, uint dwHoverTime )
            {
                cbSize = Marshal.SizeOf( typeof( TRACKMOUSEEVENT ) );
                this.dwFlags = dwFlags;
                this.hWnd = hWnd;
                this.dwHoverTime = dwHoverTime;
            }
        }

        public enum MapVirtualKeyMapTypes : uint
        {
            MAPVK_VK_TO_VSC = 0x00,
            MAPVK_VSC_TO_VK = 0x01,
            MAPVK_VK_TO_CHAR = 0x02,
            MAPVK_VSC_TO_VK_EX = 0x03,
            MAPVK_VK_TO_VSC_EX = 0x04
        }

        [DllImport( Comdlg32, CharSet = CharSet.Auto )]
        public static extern bool GetOpenFileName( [In, Out] OpenFileName ofn );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern IntPtr MonitorFromWindow( IntPtr hwnd, uint dwFlags );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool GetMonitorInfo( IntPtr hMonitor, ref MONITORINFO lpmi );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool GetWindowPlacement( IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl );

        [DllImport( User32 )]
        public static extern uint GetMessageTime();

        [DllImport( User32 )]
        public static extern uint MapVirtualKey( uint uCode, MapVirtualKeyMapTypes uMapType );

        [DllImport( User32 )]
        public static extern uint GetKeyState( uint key );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool GetCursorPos( out POINT lpPoint );

        [DllImport( User32 )]
        public static extern IntPtr SetTimer( IntPtr hWnd, IntPtr nIDEvent, uint uElapse, IntPtr lpTimerFunc );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool KillTimer( IntPtr hWnd, IntPtr uIDEvent );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern uint GetWindowThreadProcessId( IntPtr hWnd, IntPtr ProcessId );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool PeekMessage( out NativeMessage lpMsg, HandleRef hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax, PeekMessageFlags wRemoveMsg );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool PeekMessage( out NativeMessage lpMsg, IntPtr hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax, PeekMessageFlags wRemoveMsg );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool GetMessage( out NativeMessage lpMsg, HandleRef hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool GetMessage( out NativeMessage lpMsg, IntPtr hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern IntPtr DispatchMessage( [In] ref NativeMessage lpmsg );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern bool TranslateMessage( [In] ref NativeMessage lpMsg );

        [DllImport( User32 )]
        public static extern void PostQuitMessage( int nExitCode );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool PostMessage( HandleRef hWnd, uint Msg, IntPtr wParam, IntPtr lParam );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool PostThreadMessage( uint threadId, uint Msg, IntPtr wParam, IntPtr lParam );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern IntPtr SendMessage( IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern int TrackMouseEvent( ref TRACKMOUSEEVENT lpEventTrack );

        [DllImport( Kernel32 ), SuppressUnmanagedCodeSecurity]
        public static extern bool QueryPerformanceFrequency( out long frequency );

        [DllImport( Kernel32 ), SuppressUnmanagedCodeSecurity]
        public static extern bool QueryPerformanceCounter( out long lpPerformanceCount );

        [DllImport( Kernel32 ), SuppressUnmanagedCodeSecurity]
        public static extern uint GetCurrentThreadId();

        [DllImport( Kernel32 )]
        public static extern IntPtr GetModuleHandle( string lpModuleName = null );

        [DllImport( Kernel32 )]
        public static extern IntPtr GetCurrentProcess();

        [DllImport( Kernel32 )]
        public static extern bool SetProcessWorkingSetSize( IntPtr hProcess, UIntPtr
           dwMinimumWorkingSetSize, UIntPtr dwMaximumWorkingSetSize );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        public static extern IntPtr DefWindowProc( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam );

        [DllImport( User32, SetLastError = true )]
        public static extern IntPtr CreateWindowEx(
           WindowStylesEx dwExStyle,
           string lpClassName,
           string lpWindowName,
           WindowStyles dwStyle,
           int x,
           int y,
           int nWidth,
           int nHeight,
           IntPtr hWndParent,
           IntPtr hMenu,
           IntPtr hInstance,
           IntPtr lpParam );

        [DllImport( User32 )]
        public static extern IntPtr LoadCursor( IntPtr hInstance, int lpCursorName );

        [DllImport( Shell32 )]
        public static extern bool Shell_NotifyIcon( uint dwMessage, [In] ref NOTIFYICONDATA pnid );

        [DllImport( Shell32 )]
        public static extern uint DragQueryFile( IntPtr hDrop, uint iFile, [Out] StringBuilder lpszFile, uint cch );

        [DllImport( Shell32 )]
        public static extern void DragFinish( IntPtr hDrop );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool ReleaseCapture();

        [DllImport( User32 )]
        public static extern IntPtr SetCapture( IntPtr hWnd );

        [DllImport( User32 ), SuppressUnmanagedCodeSecurity]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool ValidateRect( IntPtr hWnd, IntPtr lpRect );

        [DllImport( User32 )]
        public static extern int MapWindowPoints( IntPtr hWndFrom, IntPtr hWndTo, [In, Out] ref POINT pt, [MarshalAs( UnmanagedType.U4 )] uint cPoints );

        [DllImport( User32 )]
        public static extern IntPtr SetFocus( IntPtr hWnd );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool SetWindowText( IntPtr hwnd, string lpString );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool ShowWindow( IntPtr hWnd, uint nCmdShow );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool CloseWindow( IntPtr hWnd );

        [DllImport( User32 )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool DestroyWindow( IntPtr hWnd );

        [DllImport( User32 )]
        public static extern int RegisterClassEx( [In] ref WNDCLASSEX lpwcx );

        [DllImport( User32 )]
        public static extern bool UnregisterClass( string lpClassName, IntPtr hInstance );
    }

    internal static class Utility
    {
        public static void ThrowIfZero( IntPtr value )
        {
            if ( value == IntPtr.Zero )
                throw new Exception();
        }

        public static void ThrowIfZero( int value )
        {
            if ( value == 0 )
                throw new Exception();
        }

        public static void ThrowIfNull( object value )
        {
            if ( value == null )
                throw new Exception();
        }
    }
}
