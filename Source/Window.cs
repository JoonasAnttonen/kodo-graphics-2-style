﻿using System.Runtime.InteropServices;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// A Window.
    /// </summary>
    public class Window : WindowBase
    {
        [DllImport( "Dwmapi.dll" )]
        static extern bool DwmGetColorizationColor( out uint color, out bool colorBlend );

        TextStyle textStyle = TextStyle.Default;

        SharedStyle sharedStyle;
        SolidColorBrush sharedBrush;

        ContextMenu menu;
        ContextTip defaultContextTip;

        Color clearColor = Color.LightSkyBlue;
        Rectangle clsRect;
        Rectangle minRect;
        Rectangle clientArea;

        bool mouseOnCls;
        bool mouseOnMin;
        bool mouseIsDown;

        /// <summary>
        /// Get or set the <see cref="ContextMenu"/>.
        /// </summary>
        public ContextMenu Menu
        {
            get { return menu; }
            set
            {
                if ( menu != value )
                {
                    menu = value;
                    Update();
                }
            }
        }

        /// <summary> 
        /// Get the shared text types. (DO NOT USE THIS OUTSIDE OnDraw!) 
        /// </summary>
        public TextStyle TextStyle
        {
            get { return textStyle; }
            set
            {
                textStyle = value;
                defaultContextTip.TextStyle = textStyle;
                Update();
            }
        }

        /// <summary>
        /// Get the shared solid color brush. (DO NOT USE THIS OUTSIDE OnDraw!) 
        /// </summary>
        protected internal SolidColorBrush SharedBrush
        {
            get { return sharedBrush; }
        }

        /// <summary> 
        /// Get the shared styling brushes. (DO NOT USE THIS OUTSIDE OnDraw!) 
        /// </summary>
        protected internal SharedStyle SharedStyle
        {
            get { return sharedStyle; }
        }

        /// <summary> 
        /// Get the window client <see cref="Rectangle"/>. 
        /// </summary>
        protected Rectangle Client
        {
            get { return clientArea; }
        }

        /// <summary>
        /// Create a new instance of <see cref="Window"/>.
        /// </summary>
        /// <param name="manager">The owning <see cref="WindowManager"/>.</param>
        public Window( WindowManager manager ) : this( manager, new WindowSettings() ) { }
        /// <summary>
        /// Create a new instance of <see cref="Window"/>.
        /// </summary>
        /// <param name="manager">The owning <see cref="WindowManager"/>.</param>
        /// <param name="settings">The <see cref="WindowSettings"/>.</param>
        public Window( WindowManager manager, WindowSettings settings )
            : base( manager, settings )
        {
            defaultContextTip = new ContextTip( this );
            defaultContextTip.TextStyle = textStyle;
            defaultContextTip.AlwaysOnTop = true;
            defaultContextTip.Visible = false;
        }

        /// <summary>
        /// Spawn a tooltip to the current cursor position with the specified text.
        /// </summary>
        /// <param name="text">The text to display.</param>
        public void ShowTooltip( string text )
        {
            defaultContextTip.Spawn( FromScreenToWindow( Mouse.GetPosition() ), text );
        }

        bool CheckMouse( Point point )
        {
            var lastOnCls = mouseOnCls;
            var lastOnMin = mouseOnMin;
            mouseOnCls = clsRect.Contains( point );
            mouseOnMin = minRect.Contains( point );

            return lastOnCls != mouseOnCls || lastOnMin != mouseOnMin;
        }

        /// <summary>
        /// Called when the window received a key dowm event.
        /// </summary>
        /// <param name="key">The <see cref="Key"/>.</param>
        /// <returns>Was the event handled?</returns>
        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.F4 && Keyboard.IsDown( Key.Alt ) )
            {
                Close();
            }

            return false;
        }

        /// <summary>
        /// Called when the window receives a mouse move event.
        /// </summary>
        /// <param name="mouse">The <see cref="Mouse"/>.</param>
        /// <returns>Was the event handled?</returns>
        protected override bool OnMouseMove( Mouse mouse )
        {
            if ( defaultContextTip.Visible )
            {
                defaultContextTip.Despawn();
                Draw();
            }

            return false;
        }

        /// <summary>
        /// Called when the window receives a mouse up event.
        /// </summary>
        /// <param name="mouse">The <see cref="Mouse"/>.</param>
        /// <returns>Was the event handled?</returns>
        protected override bool OnMouseUp( Mouse mouse )
        {
            if ( mouseIsDown )
            {
                if ( mouseOnMin )
                    Minimize();
                else if ( mouseOnCls )
                    Close();

                mouseIsDown = false;
                Draw();
            }

            return false;
        }

        /// <summary>
        /// Called when the window receives a mouse down event.
        /// </summary>
        /// <param name="mouse">The <see cref="Mouse"/>.</param>
        /// <returns>Was the event handled?</returns>
        protected override bool OnMouseDown( Mouse mouse )
        {
            if ( menu != null && mouse.Button == MouseButton.Right )
            {
                menu.Show( mouse.Position );
            }

            if ( mouse.Button == MouseButton.Left )
            {
                mouseIsDown = true;
            }

            return false;
        }

        /// <summary>
        /// Called when the window receives a mouse leave event.
        /// </summary>
        /// <param name="mouse">The <see cref="Mouse"/>.</param>
        /// <returns>Was the event handled?</returns>
        protected override bool OnMouseLeave( Mouse mouse )
        {
            if ( CheckMouse( mouse.Position ) )
                Draw();

            return false;
        }

        /// <summary>
        /// Called when the window can perform hit testing.
        /// </summary>
        /// <param name="point">The <see cref="Point"/>.</param>
        /// <returns>Was the hit test positive?</returns>
        protected override bool OnHitTest( Point point )
        {
            if ( CheckMouse( point ) )
                Draw();

            return mouseOnCls || mouseOnMin;
        }

        /// <summary>
        /// Called when the window need to be loaded.
        /// </summary>
        /// <param name="context">The drawing <see cref="Context"/>.</param>
        protected override void OnLoad( Context context )
        {
            sharedStyle = new SharedStyle( context );
            sharedBrush = new SolidColorBrush( context, Color.GhostWhite );
        }

        /// <summary>
        /// Called when the window needs to be updated.
        /// </summary>
        /// <param name="context">The drawing <see cref="Context"/>.</param>
        protected override void OnUpdate( Context context )
        {
            // Calculate the client area and button positions.
            var size = Area.Dimensions;

            clientArea = new Rectangle(
                Settings.Margings.Left + 1,
                Settings.Margings.Top + 1,
                Settings.Margings.Left + (size.Width - (Settings.Margings.Left + 1 + Settings.Margings.Right + 1)),
                Settings.Margings.Top + (size.Height - (Settings.Margings.Top + 1 + Settings.Margings.Bottom + 1)) );

            clsRect = new Rectangle( size.Width - 45 - 7, 0, size.Width - 7, 20 );
            minRect = new Rectangle( clsRect.Left - 2 - 22, 0, clsRect.Left - 2, 20 );

            // Get the border color from DWM.
            uint dwmColor;
            bool dwmColorBlend;
            DwmGetColorizationColor( out dwmColor, out dwmColorBlend );
            clearColor = new Color( dwmColor );
        }

        /// <summary>
        /// Called when the window needs to draw the locked overlay.
        /// </summary>
        /// <param name="context"></param>
        protected override void OnDrawLocked( Context context )
        {
            sharedBrush.Opacity = 0.5f;
            sharedBrush.Color = sharedStyle.Background.Color;
            context.FillRectangle( Client, sharedBrush );
        }

        /// <summary>
        /// Called when the window needs to drawn.
        /// </summary>
        /// <param name="context">The drawing <see cref="Context"/>.</param>
        protected override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            sharedBrush.Opacity = 1;

            // Color the border.
            sharedBrush.Color = clearColor;
            context.FillRectangle( clip, sharedBrush );

            if ( Settings.NoTitle )
                return;

            //
            // Draw the close button.
            //

            var closeSignRect = new Rectangle( new Size( 8, 7 ) ).CenterTo( clsRect );

            sharedBrush.Color = Color.IndianRed;
            context.FillRectangle( clsRect, sharedBrush );

            if ( mouseOnCls )
            {
                sharedBrush.Color = clearColor;
                context.FillRectangle( clsRect, sharedBrush );

                if ( mouseIsDown )
                    context.FillRectangle( clsRect, sharedBrush );
            }

            sharedBrush.Color = Color.White;
            context.DrawLine( closeSignRect.TopLeft, closeSignRect.BottomRight, sharedBrush, 1.5f );
            context.DrawLine( closeSignRect.BottomLeft, closeSignRect.TopRight, sharedBrush, 1.5f );

            //
            // Draw the minimize button.
            //

            if ( mouseOnMin )
            {
                sharedBrush.Color = clearColor;
                context.FillRectangle( minRect, sharedBrush );

                if ( mouseIsDown )
                    context.FillRectangle( minRect, sharedBrush );

                sharedBrush.Color = Color.White;
            }
            else
            {
                sharedBrush.Color = Color.Black;
            }

            context.DrawLine(
                new Point( minRect.Left + 7, minRect.Bottom - 6 ),
                new Point( minRect.Right - 7, minRect.Bottom - 6 ), sharedBrush, 2 );

            sharedBrush.Color = Color.DarkSlateGray;
            context.DrawRectangle( clip, sharedBrush, 2 );

            //
            // Draw the window title.
            //

            sharedBrush.Color = Color.Black;
            textStyle.Alignment = TextStyleAlignment.CenterCenter;
            context.DrawText( Title, textStyle, new Rectangle( clip.Left + 40, clip.Top, clip.Right - 80, Margins.Top ), sharedBrush );
        }
    }
}
