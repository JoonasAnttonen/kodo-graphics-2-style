﻿namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Base class for all controls.
    /// </summary>
    public abstract class Control : ControlBase
    {
        Window parent;
        Color accentColor = Color.LightSkyBlue;

        TextStyle textStyle = TextStyle.Default;

        ContextMenu menu;
        string tip;

        /// <summary>
        /// Get or set the <see cref="TextStyle"/> of the control.
        /// </summary>
        public TextStyle TextStyle
        {
            get { return textStyle; }
            set
            {
                textStyle = value;
                Update();
            }
        }

        /// <summary>
        /// Get or set the tooltip text.
        /// </summary>
        public string Tip
        {
            get { return tip; }
            set { tip = value; }
        }

        /// <summary>
        /// Get or set the <see cref="ContextMenu"/>.
        /// </summary>
        public ContextMenu Menu
        {
            get { return menu; }
            set { menu = value; }
        }

        /// <summary>
        /// Get the <see cref="Color"/> used as an accent.
        /// </summary>
        public Color AccentColor
        {
            get { return accentColor; }
            set
            {
                accentColor = value;
                Update();
            }
        }

        /// <summary>
        /// Get the <see cref="Window"/> that contains this <see cref="Control"/>.
        /// </summary>
        public new Window Parent
        {
            get { return parent; }
        }

        /// <summary> 
        /// Get the shared solid color brush. (DO NOT USE THIS OUTSIDE OnDraw!) 
        /// </summary>
        protected SolidColorBrush SharedBrush
        {
            get { return parent.SharedBrush; }
        }

        /// <summary> 
        /// Get the shared styling brushes. (DO NOT USE THIS OUTSIDE OnDraw!)
        /// </summary>
        protected SharedStyle SharedStyle
        {
            get { return parent.SharedStyle; }
        }

        /// <summary>
        /// Create a new instance of <see cref="Control"/>.
        /// </summary>
        /// <param name="window">The containing <see cref="Window"/>.</param>
        public Control( Window window )
            : base( window )
        {
            parent = window;
        }

        protected internal override void OnMouseHover( Mouse mouse )
        {
            if ( !string.IsNullOrEmpty( tip ) )
            {
                Parent.ShowTooltip( tip );
            }
        }

        protected internal override void OnMouseDown( Mouse mouse )
        {
            if ( menu != null && mouse.Button == MouseButton.Right )
            {
                menu.Show( FromControlToWindow( mouse.Position ) );
            }
        }

        protected internal override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );
        }
    }
}
