﻿using System;

namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Ultimate base class for all controls.
    /// </summary>
    public abstract class ControlBase
    {
        WindowBase parent;

        Matrix3x2 transform;
        Rectangle area;

        bool hasMouse;
        bool hasKeyboard;
        bool visible;
        bool locked;
        bool alwaysOnTop;

        internal bool HasMouse
        {
            get { return hasMouse; }
            set
            {
                if ( hasMouse != value )
                {
                    hasMouse = value;

                    if ( hasMouse )
                        OnGotMouse();
                    else
                        OnLostMouse();
                }
            }
        }

        internal bool HasKeyboard
        {
            get { return hasKeyboard; }
            set
            {
                if ( hasMouse != value )
                {
                    hasKeyboard = value;

                    if ( hasKeyboard )
                        OnGotKeyboard();
                    else
                        OnLostKeyboard();
                }
            }
        }

        internal bool RenderThisFrame;

        /// <summary>
        /// Get or set whether or not this control is always on top.
        /// </summary>
        public bool AlwaysOnTop
        {
            get { return alwaysOnTop; }
            set
            {
                alwaysOnTop = value;
                Parent.Load();
            }
        }

        /// <summary>
        /// Get or set whether or not this control is locked.
        /// </summary>
        public bool Locked
        {
            get { return locked; }
            set
            {
                if ( locked != value )
                {
                    locked = value;
                    parent.ResetFocus();
                    Parent.Update();
                }
            }
        }

        /// <summary>
        /// Get or set whether or not this control is visible.
        /// </summary>
        public bool Visible
        {
            get { return visible; }
            set
            {
                if ( visible != value )
                {
                    visible = value;

                    if ( !visible )
                        Parent.Defocus( this );

                    Parent.Update();
                }
            }
        }

        /// <summary>
        /// The translation matrix of this control.
        /// </summary>
        protected internal Matrix3x2 Transform
        {
            get { return transform; }
        }

        /// <summary>
        /// The parent of this control.
        /// </summary>
        public WindowBase Parent
        {
            get { return parent; }
        }

        /// <summary>
        /// The rectangle that describes (in window coordinates) the position and size of this control.
        /// </summary>
        public Rectangle Area
        {
            get { return area; }
            set
            {
                if ( area != value )
                {
                    area = value;
                    transform = Matrix3x2.Translation( area.Location );

                    Parent.Update();
                }
            }
        }

        /// <summary>
        /// Create a new <see cref="ControlBase"/>.
        /// </summary>
        /// <param name="window">The parent.</param>
        protected ControlBase( WindowBase window )
        {
            visible = true;
            parent = window;

            transform = Matrix3x2.Identity;
            area = new Rectangle( 0, 0, 10, 10 );

            parent.AddControl( this );
        }

        /// <summary> Graphical update (redraw) of the Control. </summary>
        protected void Refresh() { parent.Draw( this ); }

        /// <summary> Logical update (update and redraw) of the Control. </summary>
        protected void Update() { parent.Update( this ); }

        /// <summary> Full update (reload assets, update and redraw) of the Control. </summary>
        protected void Load() { parent.Load( this ); }

        /// <summary> Enable the control to receive keyboard input. </summary>
        public void CaptureKeyboard() { parent.FocusKeyboard( this ); }
        /// <summary> Capture all mouse input to the control. </summary>
        public void CaptureMouse() { parent.FocusMouse( this ); }

        protected internal virtual ContextMenu OnContextMenu( Point point ) { return null; }

        /// <summary> Called when the mouse enters the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseEnter( Mouse mouse ) { }
        /// <summary> Called when the mouse hovers the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseHover( Mouse mouse ) { }
        /// <summary> Called when the mouse moves on the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseMove( Mouse mouse ) { }
        /// <summary> Called when the mouse leaves the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseLeave( Mouse mouse ) { }
        /// <summary> Called when a mouse button is released in the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseUp( Mouse mouse ) { }
        /// <summary> Called when a mouse button is pressed in the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseDown( Mouse mouse ) { }
        /// <summary> Called when the mouse wheel is used in the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseWheel( Mouse mouse ) { }
        /// <summary> Called when a mouse button is double clicked in the control. </summary>
        /// <param name="mouse">The mouse data.</param>
        protected internal virtual void OnMouseDoubleClick( Mouse mouse ) { }

        /// <summary> Called when the control receives mouse focus. </summary>
        protected internal virtual void OnGotMouse() { }
        /// <summary> Called when the control loses mouse focus. </summary>
        protected internal virtual void OnLostMouse() { }

        /// <summary> Called when the control receives keyboard focus. </summary>
        protected internal virtual void OnGotKeyboard() { }
        /// <summary> Called when the control loses keyboard focus. </summary>
        protected internal virtual void OnLostKeyboard() { }

        /// <summary> Called when a keyboard key is released.  </summary>
        protected internal virtual void OnKeyUp( Key key ) { }
        /// <summary> Called when a keyboard key is pressed. </summary>
        protected internal virtual void OnKeyDown( Key key ) { }
        /// <summary> Called when a input character arrives.  </summary>
        protected internal virtual void OnCharacter( Char c ) { }

        /// <summary> Map control coordinates to windows coordinates. </summary>
        /// <param name="point"> The <see cref="Point"/> to map. </param>
        protected internal Point FromControlToWindow( Point point ) { return new Point( Area.Left + point.X, Area.Top + point.Y ); }
        /// <summary> Map window coordinates to control coordinates. </summary>
        /// <param name="point"> The <see cref="Point"/> to map. </param>
        protected internal Point FromWindowToControl( Point point ) { return new Point( point.X - Area.Left, point.Y - Area.Top ); }

        /// <summary> Map screen coordinates to control coordinates. </summary>
        /// <param name="point"> The <see cref="Point"/> to map. </param>
        protected internal Point FromScreenToControl( Point point ) { return FromWindowToControl( FromScreenToWindow( point ) ); }

        /// <summary> Map screen coordinates to window coordinates. </summary>
        /// <param name="point"> The <see cref="Point"/> to map. </param>
        protected internal Point FromScreenToWindow( Point point ) { return parent.FromScreenToWindow( point ); }
        /// <summary> Map window coordinates to screen coordinates. </summary>
        /// <param name="point"> The <see cref="Point"/> to map. </param>
        protected internal Point FromWindowToScreen( Point point ) { return parent.FromWindowToScreen( point ); }

        /// <summary> Called when the control is loaded. </summary>
        /// <param name="context">The drawing context.</param>
        protected internal virtual void OnLoad( Context context ) { }
        /// <summary> Called when the control is updated. </summary>
        /// <param name="context">The drawing context.</param>
        protected internal virtual void OnUpdate( Context context ) { }
        /// <summary> Called when the control is drawn. </summary>
        /// <param name="context">The drawing context.</param>
        protected internal virtual void OnDraw( Context context ) { }
    }
}
