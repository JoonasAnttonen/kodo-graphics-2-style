﻿namespace Kodo.Graphics.Style
{
    /// <summary>
    /// Represents the initial settings for a <see cref="WindowBase"/>.
    /// </summary>
    public class WindowSettings
    {
        //public static readonly WindowSettings BorderOnlyToolWindow = new WindowSettings { Margings = new WindowMargings(3), ToolWindow = true, NoTitle = true };
        /// <summary>
        /// Name and title of the window.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Area of the window, in screen coordinates.
        /// </summary>
        public Rectangle Area { get; set; } = new Rectangle( 500, 500, 500 + 500, 500 + 500 );

        /// <summary>
        /// Margings (border) of the window.
        /// </summary>
        public WindowMargings Margings { get; set; } = new WindowMargings( 3, 30, 3, 3 );

        /// <summary>
        /// Minimum size of the window.
        /// </summary>
        public Size MinimumSize { get; set; } = new Size( 300, 300 );

        /// <summary>
        /// Whether or not the window accepts drag and drops of files.
        /// </summary>
        public bool AcceptFiles { get; set; } = false;

        /// <summary>
        /// Whether or not the window is a tool window.
        /// </summary>
        public bool ToolWindow { get; set; } = false;

        /// <summary>
        /// Whether or not the window has a title bar and controls.
        /// </summary>
        public bool NoTitle { get; set; } = false;
    }
}
